<?php



// list of php files (classes...) needed for this class :
require_once "staff_service.php";

/**
 * Class "staff_list" / file "staff_list.php"
 *
 * Your class description here ...
 *
 * This class requires the use of the class:
 *  • require_once "staff_list.php";
 *
 * @subpackage satff list
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class staff_list
{

	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array 
	 */
	public $aResult = [];

	/**
	 * @property object $oService The class service object.
	 */
	private $oService;

	/**
	 * Constructor :
	 * 
	 * it is executed thanks to code "new staff_service()"
	 * every time it is executed it creates an instance of this class
	 * and it executes the code present in this function
	 */
	public function __construct()
	{
		// execute main function
		$this->main();
	}

	//----------------------------------------------------------------------------------//
	//                                 MAIN FUNCTION                                    //
	//----------------------------------------------------------------------------------//

	/**
	 * Your main function : Describe here what it is used for...
	 */
	private function main()
	{
		// I create my service object
		$this->oService = new Staff_service();
		$this->oService->staff_list();

		// i pass my parameters to have access to them in my html pages
		$this->aResult = $this->oService->aResult;
		// I pass my parameters to have access to them in my HTML pages
		$this->HTML_DATAS = $this->oService->HTML_DATAS;

		// kill object
		unset($this->oService);
	}
}
