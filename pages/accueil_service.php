<?php
require_once "active.php";
/**
 * Class  Accueil_service | file Accueil_service.php
 * 
 * In this class,we have methods for:
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "Accueil_service.php"
 * 
 * @subpackage Accueil service
 * @author Ilyes
 * @package Portfolio Project
 * @copyright 2021
 * @version v1.0
 */

class Accueil_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }
}
