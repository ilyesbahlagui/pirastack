<?php

/**
 * Class Security | file security.php
 *
 * In this class, we find all about security
 *
 * List of classes needed for this class
 * no required 
 * 
 *
 * @subpackage security 
 * @author Jijou
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */

class Security
{
	/**
	 * Verify $_GET and $_POST
	 * Create a new public variable $HTML_DATAS filtered by htmlspecialchars
	 * Contain all request data (GET OR POST) 
	 *
	 * @var array $HTML_DATAS
	 */
	public $HTML_DATAS;

	function __construct()
	{
		// put all variables $_POST et $_GET into the array $HTML_DATAS
		$this->HTML_DATAS = [];

		foreach ($_POST as $key => $val) {
			/**
			 * strstr example:
			 * $email  = 'name@example.com';
			 * $domain = strstr($email, '@');
			 * echo $domain; // Affiche : @example.com
			 * 
			 * strolower return string tiny
			 * 
			 *  $chaine = "<p> ceci est un paragraphe</p>";
			 *  echo htmlspecialchars($chaine, ENT_QUOTES);
			 * Result :
			 * 	&lt;p&gt; ceci est un paragraphe&lt;/p&gt;
			 */
			
			if (strstr(strtolower($val), "<script")) {
				$val = str_replace("<script", "<em", strtolower($val));
				$val = str_replace("</script>", "</em>", strtolower($val));
			}
			$val = str_replace("__APOSTROPHE__", "\"", $val);
			$this->HTML_DATAS[$key] = htmlspecialchars($val, ENT_QUOTES);
					
		}

		foreach ($_GET as $key => $val) {
			if (strstr(strtolower($val), "<script")) {
				$val = str_replace("<script", "<em", strtolower($val));
				$val = str_replace("</script>", "</em>", strtolower($val));
			}
			$val = str_replace("__APOSTROPHE__", "\"", $val);
			$this->HTML_DATAS[$key] = htmlspecialchars($val, ENT_QUOTES);
		}

		if ((!(isset($this->HTML_DATAS["page"]))) || ($this->HTML_DATAS["page"] == "")) {
			$this->HTML_DATAS["page"] = "accueil";
		}
	}
	//End of class	
}
