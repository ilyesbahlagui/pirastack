<?php
require_once "active.php";
/**
 * Class  Product_service | file product_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method product_list()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "product_service.php"
 * 
 * @subpackage product service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Product_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }



  public function product_list()
  {
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "product_list.sql";

    $this->aResult=$this->oDatabase->getSelectDatas($spathSQL, array(
      "id_produit" => $_SESSION["id_produit"],

    ));
    
  }
}
