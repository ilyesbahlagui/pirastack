<?php
require_once "active.php";
/**
 * Class  Shop_gestion_service | file shop_gestion_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method shop_gestion_service()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "shop_gestion_service.php"
 * 
 * @subpackage Shop_gestion_service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Shop_gestion_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;

  /**
   * public $error && $UploadOK is boolean 
   * @var boolean
   */
  public $error;
  public $UploadOK;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }

  // Method list shop && category && tva
  public function shop_gestion_list()
  {

    // select shop global article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_gestion_list_select.sql";

    $this->aResult["shop"] = $this->oDatabase->getSelectDatas($spathSQL, array());
    // select list famille article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "category_list_select.sql";

    $this->aResult["categorie"] = $this->oDatabase->getSelectDatas($spathSQL, array());

    // select list tva 
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "tva_list_select.sql";

    $this->aResult["tva"] = $this->oDatabase->getSelectDatas($spathSQL, array());
  }
  // ***********************************************************************************************************************************//
  //                                                          Insert Article                                                             //
  // ***********************************************************************************************************************************// 
  // Method add article 
  public function shop_gestion_insert()
  {
    // call the method control value 
    $this->ControlRegex();
    // ***********************************************//
    //        CONTROL INPUT OK and Insert FILE OK     //
    // ***********************************************//
    error_log("test upload =>" . $this->UploadOK);

    // JE PREFERE FAIRE LE INSERT DE IMAGE SI LE CONTROL DE VALEUR EST BON CAR COMME CA JE POURRAIS ETRE SUR QUE LE INSERT DU PRODUIT DANS LA BDD SERA BON D'AVANCE
    // ET QUE J'AI LA SECURITER DE L'INSERT DE IMAGE AVEC!!!
    // CONTROL OK CALL METHOD INSERT FILE 
    if ($this->error == 0) {
      $this->InsertFile();
    }
    // THE INSERT FILE IS OK AND CONTROL INPUT IS OK
    if ($this->error == 0 && $this->UploadOK == 1) {

      error_log("test upload =>" . $this->UploadOK);
      $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_gestion_insert.sql";

      $this->oDatabase->treatDatas($spathSQL, array(
        "nom_produit" => $this->HTML_DATAS["nom_produit"],
        "prix" => $this->HTML_DATAS["prix"],
        "stock" => $this->HTML_DATAS["stock"],
        "description" => $this->HTML_DATAS["description"],
        "id_tva" => $this->HTML_DATAS["id_tva"],
        "id_categorie" => $this->HTML_DATAS["id_categorie"],
      ));
      // foreach ($aRegex as $key => $value) {D
      //   error_log($key . " => " . $value);
      // }

      error_log("le controle est bon");
      $this->aResult["error"] = $this->error;
      // get last id for insert in the aarray js so delete with id 
      $this->aResult["id"] = $this->oDatabase->getLastInsertId();
      $this->aResult["upload"] = $this->UploadOK;
    } else {
      error_log("le controle est mauvais");
      $this->aResult["error"] = $this->error;
      $this->aResult["upload"] = $this->UploadOK;
    }
  }

  // ***********************************************************************************************************************************//
  //                                                          UPDATE Article                                                             //
  // ***********************************************************************************************************************************// 
  // method update article
  public function shop_gestion_update()
  {
    // call the method control value from html 
    $this->ControlRegex();
    // ******************************//
    //        CONTROL INPUT OK      //
    // ******************************//
    if ($this->error == 0) {

      $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_gestion_update.sql";

      $this->oDatabase->treatDatas($spathSQL, array(
        "nom_produit" => $this->HTML_DATAS["nom_produit"],
        "id_produit" => $this->HTML_DATAS["id_produit"],
        "prix" => $this->HTML_DATAS["prix"],
        "stock" => $this->HTML_DATAS["stock"],
        "description" => $this->HTML_DATAS["description"],
        "id_tva" => $this->HTML_DATAS["id_tva"],
        "id_categorie" => $this->HTML_DATAS["id_categorie"],
      ));
      // RENAME IMAGE FILE
      $sNameFileAfter = $this->GetNameFile($this->HTML_DATAS["nom_produit"]);
      $sNameFileBefore = $this->GetNameFile($this->HTML_DATAS["nom_produit_before"]);
      $PathNewFile = 'assets/image/' . $sNameFileAfter . '.jpg';
      $PathBeforeFile = 'assets/image/' . $sNameFileBefore . '.jpg';
      rename($PathBeforeFile, $PathNewFile);



      error_log("le controle est bon");
      $this->aResult["error"] = $this->error;
    } else {
      error_log("le controle est mauvais");
      $this->aResult["error"] = $this->error;
    }
  }
  // ***********************************************************************************************************************************//
  //                                                         DELETE ARTICLE                                                       //
  // ***********************************************************************************************************************************// 
  // method delete the product in the table produit 
  public function shop_gestion_delete()
  {
    // $search content the all letter with accent
    $search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
    // $replace content the all letter with not accent
    $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');

    $sNameFile = strtolower($this->HTML_DATAS["nom_produit"]);
    $sNameFile = str_replace($search, $replace, $sNameFile);
    $sNameFile = str_replace(" ", "-", $sNameFile);
    error_log($sNameFile);
    // path FILE
    $PathFile = 'assets/image/' . $sNameFile . '.jpg';
    // file exist so kill file image
    if (file_exists($PathFile)) {
      unlink($PathFile);
      $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_gestion_delete.sql";

      $this->oDatabase->treatDatas($spathSQL, array(
        "id_produit" => $this->HTML_DATAS["id_produit"],
      ));

      $this->aResult = 1;
      error_log("file exist and delete product ");
    }
    // else no kill file image  
    else {
      error_log("file exist");
      $this->aResult = 0;
    }
  }

  // ***********************************************************************************************************************************//
  //                                                          CONTROL DATA                                                         //
  // ***********************************************************************************************************************************// 
  // the method control the value from html with regex 
  public function ControlRegex()
  {
    $bValidate = true;
    $this->error = 0;

    // the loop checked in HTM_DATAS if content value is null or undefined and return false 
    foreach ($this->HTML_DATAS as $key => $value) {
      if ($value == "" || $value == null) {
        $bValidate = false;
      }
    }


    // if the HTML_DATAS no content value null or undefined  so declared value
    if ($bValidate) {
      // declared values 
      $sNameProduct = $this->HTML_DATAS["nom_produit"];
      $iPrice = $this->HTML_DATAS["prix"];
      $iStock = $this->HTML_DATAS["stock"];
      $sDescription = $this->HTML_DATAS["description"];
      $iIdTva = $this->HTML_DATAS["id_tva"];
      $iIdCategorie = $this->HTML_DATAS["id_categorie"];


      // Is array with regex for create loop why tests the value 
      $aRegex = array(
        "nom_produit" => preg_match("/^([a-zA-Z-.|\s]{0,20})$/", $sNameProduct),
        "prix" => preg_match("/^[0-9]{0,5}$/", $iPrice),
        "stock" => preg_match("/^[0-9]{0,4}$/", $iStock),
        "tva" => preg_match("/^([0-9]{1,2})$/", $iIdTva),
        "categorie" => preg_match("/^([0-9]{1,2})$/", $iIdCategorie),
        "description" => preg_match("/^[a-zA-Z0-9().,?!|\s]{0,500}$/", $sDescription),
      );

      // in the array tests the boolean value 
      foreach ($aRegex as $key => $value) {
        error_log($key . " => " . $value);
        // if the value is false so $error =1
        if ($value == 0) {
          $this->error = 1;
        }
      }
    }
    //else the HTML_DATAS content value null or undefined  so declared $error=1;
    else {
      $this->error = 1;
    }
  }

  // ***********************************************************************************************************************************//
  //                                                          FILE Insert                                                           //
  // ***********************************************************************************************************************************// 

  public function InsertFile()
  {
    //  call the method GetNameFile get name file before =>'Sweat à capuche' after => 'sweat-a-capuche'
    $sNameFile = $this->GetNameFile($this->HTML_DATAS["nom_produit"]);

    error_log($sNameFile);

    // List of extensions OK
    $aExtension_valide = array('jpeg', 'jpg', 'png');
    // SIZE
    $iSizeFile = $_FILES["files"]["size"];
    // TYPE 
    // strrchr => image/jpg = /jpg
    // substr =>/jpg = jpg
    $iTypeFile = substr(strrchr($_FILES["files"]["type"], '/'), 1);
    error_log($iTypeFile);
    // PATH
    // basename can prevent attack file system
    // by default exetnsion jpg
    $UploadFile = 'assets/image/' . basename($sNameFile . ".jpg");
    // if SIZE <500000 
    if ($iSizeFile < 500000) {
      // test file extensions 
      if (in_array($iTypeFile, $aExtension_valide)) {

        if (move_uploaded_file($_FILES['files']['tmp_name'], $UploadFile)) {
          // UPLOAD OK
          error_log("UPLOAD OK");
          $this->UploadOK = 1;
        } else {
          // UPLOAD NO OK
          error_log("UPLOAD NO OK");
          $this->UploadOK = 0;
        }
      } else {
        // EXTENSION NO OK
        error_log("EXTENSION NO OK");
        $this->UploadOK = 2;
      }
    } else {
      // SIZE NO OK
      error_log("SIZE NO OK");

      $this->UploadOK = 3;
    }
  }
  // 
  private function GetNameFile($sName)
  {
    // $search content the all letter with accent
    $search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
    // $replace content the all letter with not accent
    $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');

    $sName = strtolower($sName);
    $sName = str_replace($search, $replace, $sName);
    $sName = str_replace(" ", "-", $sName);
    error_log($sName);
    return $sName;
  }
}
