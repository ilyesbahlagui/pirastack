<?php
require_once "active.php";
/**
 * Class  Panier_service | file Panier_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method list_search()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "Panier_service.php"
 * 
 * @subpackage panier service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Panier_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;
  /**
   * public $aPanier content all id_produit and quantity product without duplicate product
   * @var array
   */
  public $aPanier;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {

    if (!isset($_SESSION)) {
      session_start();
    }
    if (!isset($_SESSION["panier"])) {
      $_SESSION["panier"] = [];
    }


    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
    $this->aPanier = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }

  public function panier_add()
  {

    // count array $_SESSION["panier"]
    $NbPanier = count($_SESSION["panier"]);
    // value that increments if the id exists
    $iExist = 0;

    // the new id select
    $new_id = $this->HTML_DATAS["id_produit"];

    for ($i = 0; $i < $NbPanier; $i++) {
      // if id exist in array so add the quantity in index where the new_id=last_id
      if ($new_id == $_SESSION["panier"][$i]["id_produit"]) {
        $iExist++;
        $_SESSION["panier"][$i]["quantity"] = $this->HTML_DATAS["quantity"] + $_SESSION["panier"][$i]["quantity"];
        error_log("new id : " . $new_id . " id " . $_SESSION["panier"][$i]["id_produit"] . " qtt " .  $_SESSION["panier"][$i]["quantity"]);
      }
    }

    // if the id not exist so add the new id in the array 
    if ($iExist == 0) {
      $_SESSION["panier"][$NbPanier] = [];
      $_SESSION["panier"][$NbPanier]["id_produit"] = $this->HTML_DATAS["id_produit"];
      $_SESSION["panier"][$NbPanier]["quantity"] = $this->HTML_DATAS["quantity"];
    }


    $this->aResult = $_SESSION["panier"];
  }



  public function panier_list()
  {

    $id_list = "";

    // This Loop create id list for the SQL request and the condition "WHERE id in(id_list)"
    for ($i = 0; $i < count($_SESSION["panier"]); $i++) {

      if (($i != count($_SESSION["panier"]) - 1) && count($_SESSION["panier"]) != 1) {
        $id_list .= $_SESSION["panier"][$i]["id_produit"] . ",";
      } else {
        $id_list .= $_SESSION["panier"][$i]["id_produit"];
      }
    }
    error_log($id_list);


    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "panier_list.sql";

    $this->aResult["article"] = $this->oDatabase->getSelectDatas($spathSQL, array(
      "id_produit" => $id_list,
    ));
    $this->aResult["panier"] = $_SESSION["panier"];
  }

  public function panier_delete()
  {
    $id_product = $this->HTML_DATAS["id_produit"];
    // $iIndice = $this->HTML_DATAS["iIndice"];
    $error="";
    // error_log("indice ".$iIndice." id ".$id_product);
    // array_splice($_SESSION["panier"], $iIndice, 1);

    for ($i = 0; $i < count($_SESSION["panier"]); $i++) {

      if ($id_product == $_SESSION["panier"][$i]["id_produit"]) {
        $iIndice=$i;
        $error=1;
      }
      
    error_log("l'id => ".$_SESSION["panier"][$i]["id_produit"]);

    }
    array_splice($_SESSION["panier"], $iIndice, 1);
  
    $this->aResult = $error;

  }
}
