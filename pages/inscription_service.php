<?php
require_once "active.php";
/**
 * Class  Inscription_service | file Inscription_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method inscription_insert()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "Inscription_service.php"
 * 
 * @subpackage inscrition service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Inscription_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }

  public function inscription_insert()
  {

    $bValidate = true;
    $aNameErrorInput = [];
    $error = 0;

    // the loop checked in HTM_DATAS if content value is null or undefined and return false 
    foreach ($this->HTML_DATAS as $key => $value) {
      if ($value == "" || $value == null) {
        $bValidate = false;
        $aNameErrorInput[count($aNameErrorInput)] = $key;
      }
    }

    // test password1 and password2

    if ($this->HTML_DATAS["password1"] !== $this->HTML_DATAS["password2"]) {
      $bValidate = false;
      $aNameErrorInput[count($aNameErrorInput)] = "password1";
      $aNameErrorInput[count($aNameErrorInput)] = "password2";
    }

    // if the HTML_DATAS no content value null or undefined  so declared value
    if ($bValidate) {
      // declared values 
      $sName = $this->HTML_DATAS["name"];
      $sFirstname = $this->HTML_DATAS["firstname"];
      $iNumber = $this->HTML_DATAS["number"];
      $Date = $this->HTML_DATAS["date"];
      $bGenre = $this->HTML_DATAS["genre"];
      $sEmail = $this->HTML_DATAS["email"];
      $sPassword = $this->HTML_DATAS["password2"];


      // Is array with regex for create loop why tests the value 
      $aRegex = array(
        "email" => preg_match("/^\w+([\.-]?\w)+@\w+([\.-]?\w+)+(\.\w{2,3})+$/", $sEmail),
        "password" => preg_match("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%.^&*-])(?!.*\s).{8,}$/", $sPassword),
        "name" => preg_match("/^([a-zA-Z]{0,10})$/", $sName),
        "firstname" => preg_match("/^([a-zA-Z]{0,10})$/", $sFirstname),
        "number" => preg_match("/^(0[1-8])?\d{8}$/", $iNumber),
        "date" => preg_match("/^\d{4}-\d{2}-\d{2}$/", $Date),
      );

      // in the array tests the boolean value 
      foreach ($aRegex as $key => $value) {
        // error_log($key . " => " . $value);
        // if the value is false so $error =1
        if ($value == 0) {
          $error = 1;
          $aNameErrorInput[count($aNameErrorInput)] = $key;
        }
      }
    }
    //else the HTML_DATAS content value null or undefined  so declared $error=1;
    else {
      $error = 1;
    }

    // ******************************//
    //        CONTROL INPUT OK      //
    // ******************************//
    if ($error == 0) {

      $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "inscription_insert.sql";

      $this->oDatabase->treatDatas($spathSQL, array(
        "name" => $sName,
        "firstname" => $sFirstname,
        "number" => $iNumber,
        "date" => $Date,
        "genre" => $bGenre,
        "email" => $sEmail,
        // crypte the password for insert in the bdd 
        "password" => password_hash($sPassword, PASSWORD_DEFAULT),
      ));

      // foreach ($aRegex as $key => $value) {
      //   error_log($key . " => " . $value);
      // }

      error_log("le controle est bon");
      $this->aResult = 1;
    } else {
      error_log("le controle est mauvais");
      $this->aResult = $aNameErrorInput;
    }
  }
}
