<?php
require_once "database.php";
require_once "security.php";

/**
 * /**
 * Class "Active" / file "active.php"
 *
 * this class instance the class security and database from database.php && security.php
 *
 *
 * @package piraStack Project
 * @author @Jijou
 * @copyright  2021
 * @version v1.0
 */
class Active{
	/**
	 * @var object $oSecurity
	 * @var object $oDatabase
	 * @var array $Data_ini
	 * @var array $HTML_DATAS
	 *
	 */
	// Database instance object
	public $oDatabase;
	// All datas from  INI file
	public $Data_ini;
	// Security instance object
	private $oSecurity;
	// Array of data from html ($_POST && $_GET)
	public $HTML_DATAS;
	
	/**
	 * 
	 * $Data_ini contain path of file 
	 * Do the connection to the database and secure the values from html for create the array with the datas
	 * 
	 */
	public function __construct()	{
		// Instance of configuration 
		$this->Data_ini= Configuration::getIniFile();

		// Instance of Database && give the datas to connection to the database in parameter
		$this->oDatabase = new Database($this->Data_ini["DB_HOST"],
								   $this->Data_ini["DB_NAME"],
								   $this->Data_ini["DB_LOGIN"],
								   $this->Data_ini["DB_PSW"]);

		// Instance of Security to have $this->HTML_DATAS
		$this->oSecurity= new Security();
		$this->HTML_DATAS= $this->oSecurity->HTML_DATAS;
	}
	/**
	 * Destroy security Object and Database Object
	 */
	public function __destruct()	{
		// destroy Instance of Security
		unset($this->oSecurity);
		// disconnect of Database
		// destroy Instance of Database
		unset($this->oDatabase);
	}

//End of class
}
