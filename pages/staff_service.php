<?php
require_once "active.php";
/**
 * Class  Staff_service | file staff_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method list_search()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "staff_service.php"
 * 
 * @subpackage staff service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Staff_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }

  public function staff_list()
  {
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "staff_list.sql";

    $this->aResult = $this->oDatabase->getSelectDatas($spathSQL, array());
  }
}