<?php

/**
 * Class "Database" / file "database.php"
 *
 *
 * Connection to the database in the constructor
 *
 * Disconnection to the database in the destructor
 *
 * Have the last insert id
 *
 * Execute select method
 *
 * Execute insert / update / delete method
 *
 * @subpackage Database
 * @author Jijou
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */

class Database
{

    /**
     * @var object $connect_bdd This private var is used to connect to the database
     */
    private $connect_bdd;

    /**
     * Connect to the database
     * 
     * @param string $host The host
     * @param string $name The name
     * @param string $login The login
     * @param string $psw The password
     */
    function __construct(string $host, string $name, string $login, string $psw)
    {
        // Connection to Database : SERVEUR / LOGIN / PASSWORD / NOM_BDD

        // the try catch execptions is used when an error is present during the execution of the PDO in the try block then php will execute the code present in the catch 
        // and print the error emitted by PDO and read it with getMessage ();
        try {
            $this->connect_bdd = new PDO('mysql:host=' . $host . ';dbname=' . $name . ';charset=utf8', $login, $psw);
        } catch (PDOException $e) {
            error_log("PDOException Connection to DB = " . $e->getMessage());
        }
    }


    /**
     * Disconnect from the database
     *
     */
    function __destruct()
    {
        $this->connect_bdd = null;
    }

    /**
     * needed for phpunit
     *
     */
    function prepare($sql)
    {
        return $this->connect_bdd->prepare($sql);
    }
    /**
     * Get the last id inserted
     *
     */
    public function getLastInsertId()
    {
        return $this->connect_bdd->lastInsertId();
    }

    /**
     * Execute select method
     * 
     * @param string $spathSQL The sql file path
     * @param array $data All the strings you want to replace and their corresponding value
     * @param int|null $bForJS Is it for javaScript ? If true, If true, some special characters will be replaced
     * 
     * @return array
     */
    function getSelectDatas(string $spathSQL, array $data = array(), $bForJS = null)
    {
        // content of SQL file
        $sql = file_get_contents($spathSQL);

        // replace variables @variable from sql by values of the same variables'name
        // protection of SQL injection 
        foreach ($data as $key => $value) {
            $value = str_replace("'", "__SIMPLEQUOT__", $value);
            $value = str_replace('"', '__DOUBLEQUOT__', $value);
            $value = str_replace(";", "__POINTVIRGULE__", $value);
            $sql = str_replace('@' . $key, $value, $sql);
            // error_log("key = " . $key . " | " . "value= " . $value. " | " . "sql = " . $sql);
        }

        error_log("getSelectDatas = " . str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", " ", $sql))));

        $resultat = [];
        $resultat["error"] = "";
        try {
            // Prepare the request a do and return an object
            $results_db = $this->connect_bdd->prepare($sql);
            // execute the prepared request
            $results_db->execute();

        } catch (PDOException $e) {
            $resultat["error"] = $e->getMessage();
            error_log("PDOException getSelectDatas = " . $resultat["error"]);
        }


        // if the request is executed
        if ($resultat["error"] == "") {
            error_log("request ok ");

            $resultat = [];
            while ($ligne = $results_db->fetch()) {

                $new_ligne = [];
                foreach ($ligne as $key => $value) {
                    // if is not numeric var 
                    if (!(is_numeric($key))) {
                        // error_log("getSelectDatas DETAILS = " . $key . " => " . $value);
                        $value = str_replace("__SIMPLEQUOT__", "'", $value);
                        $value = str_replace('__DOUBLEQUOT__', '"', $value);
                        $value = str_replace("__POINTVIRGULE__", ";", $value);
                        $new_ligne[$key] = $value;
                        error_log("la clé = " . $key . " //   la valeur = " . $value);
                    }
                }
                $resultat[] = $new_ligne;
            }
        }

        return $resultat;
    }

    /**
     * Execute insert / update / delete method
     *
     * @param string $spathSQL The '.sql' file path
     * @param array $data All the strings you want to replace and their corresponding value
     * 
     * @return array
     */
    function treatDatas(string $spathSQL, array $data = array())
    {
        // content of SQL file
        $sql = file_get_contents($spathSQL);

        // replace variables @variable from sql by values of the same variables'name
        // protection of SQL injection 
        foreach ($data as $key => $value) {
            $value = str_replace("'", "__SIMPLEQUOT__", $value);
            $value = str_replace('"', '__DOUBLEQUOT__', $value);
            $value = str_replace(";", "__POINTVIRGULE__", $value);
            $sql = str_replace('@' . $key, $value, $sql);
        }

        error_log("treatDatas = " . str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", " ", $sql))));

        // execute the request and return an object
        $resultat = [];
        $resultat["error"] = "";
        try {
            $this->connect_bdd->query($sql);
        } catch (PDOException $e) {
            $resultat["error"] = $e->getMessage();
            error_log("PDOException treatDatas = " . $resultat["error"]);
        }

        return $resultat;
    }

}
