<?php
require_once "configuration.php";
require_once "database.php";
require_once "security.php";
require_once "active.php";
use PHPUnit\Framework\TestCase;

class Product_selectTest extends TestCase
{
    /** @test */
    public function product_select()
    {
        $object = new Active();
        $sql = "SELECT id_produit, id_famille_fk, id_tva_fk, nom_produit, prix_unitaire as prix ,description_produit as description ,tva.taux_tva as tva, 
        famille.nom_famille as nom_categorie, stock_produit as stock
        FROM produit 
        inner join famille on famille.id_famille=produit.id_famille_fk
        inner join tva on tva.id_tva=produit.id_tva_fk
        WHERE 1
        order by id_famille_fk ";

        $CountLineSql = 0;
        // Execute the query in NATIVE SQL CODE
        $results_db = $object->oDatabase->prepare($sql);
        $results_db->execute();

        // Fetch the result BDD and count number line
        while ($ligne = $results_db->fetch()) {
            $CountLineSql++;
        }
        // execute the query
        $spathSQL = "../" . $object->Data_ini["PATH_SQL"] . "shop_gestion_list_select.sql";
        error_log($spathSQL);
        $aResult=$object->oDatabase->getSelectDatas($spathSQL, array());

        // checks the count of the variables and return true if is equal
		$this->assertCount($CountLineSql, $aResult );
    }
}
