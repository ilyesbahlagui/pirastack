<?php
require_once "configuration.php";
require_once "database.php";
require_once "security.php";
require_once "active.php";

use PHPUnit\Framework\TestCase;

class Product_insertTest extends TestCase
{
    /** @test */
    public function product_insert()
    {
        $object = new Active();
        $sql = "SELECT count(*) from produit ";
        
        $CountLineSql = 0;
        // Execute the query in NATIVE SQL CODE
        $results_db = $object->oDatabase->prepare($sql);
        $results_db->execute();
        // Fetch the result BDD and count number line
        while ($ligne = $results_db->fetch()) {
            $CountLineSql++;
        }
        $spathSQL = "../" . $object->Data_ini["PATH_SQL"] . "shop_gestion_insert.sql";

        $object->oDatabase->treatDatas($spathSQL, array(
            "nom_produit" => "TEST produit",
            "prix" => 15,
            "stock" => 100,
            "description" => "Test case PHP Unit",
            "id_tva" => 2,
            "id_categorie" => 3,
        ));
        $CountAfterLineSql = 0;
        // Execute the query in NATIVE SQL CODE after insert
        $results_db = $object->oDatabase->prepare($sql);
        $results_db->execute();
        // Fetch the result BDD and count number line

        while ($ligne = $results_db->fetch()) {
            $CountAfterLineSql++;
        }
        // Checks the variables if the $CountAfterLineSql is greater than +1 of $CountLineSql return true 
		$this->assertEquals($CountLineSql, $CountAfterLineSql );
    }
}
