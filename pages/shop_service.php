<?php
require_once "active.php";
/**
 * Class  Shop_service | file shop_service.php
 * 
 * In this class,we have methods for:
 * -listing all ad with method service()
 * with this interface, we'll be able to list all the add stored in database
 * 
 * List of classes needed for this class 
 * 
 * require_once "shop_service.php"
 * 
 * @subpackage shop service
 * @author Ilyes
 * @package piraStack Project
 * @copyright 2021
 * @version v1.0
 */
class Shop_service extends Active
{

  /**
   * public $resultat is used to store all datas needed for HTML templates
   * @var array
   */
  public $aResult;


  /**
   * Call the arent constructor
   *
   * init variables resultat
   */
  public function __construct()
  {
    // Call Parent Constructor
    parent::__construct();
    $this->aResult = [];
  }
  public function __destruct()
  {
    // Call Parent destructor
    parent::__destruct();
  }






  public function shop_global_list()
  {
    // select category
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "category_list_select.sql";
    $this->aResult["category"] = $this->oDatabase->getSelectDatas($spathSQL, array());


    // select count article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_global_count_select.sql";

    $this->aResult["count"] = $this->oDatabase->getSelectDatas($spathSQL, array());

    //  is the total number of articles
    $nb_Total_Art = $this->aResult["count"][0]["nbArt"];

    // is the number of articles by page
    $nb_Page_Art = 10;

    // Calculate the total number Page and round to the hight value
    $nb_Total_Page = $nb_Total_Art / $nb_Page_Art;
    $nb_Total_Page = ceil($nb_Total_Page);

    // is the page number
    $numPage = $this->HTML_DATAS["num_page"];

    // get the n artciles from page n
    $nb_Art = ($numPage - 1) * $nb_Page_Art;

    error_log("nb total page" . $nb_Total_Page);
    $this->aResult["pagination"] = $nb_Total_Page;

    // select shop article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_global_list_select.sql";

    $this->aResult["shop"] = $this->oDatabase->getSelectDatas($spathSQL, array(
      "nb_Art" => $nb_Art,
      "nb_Page_Art" => $nb_Page_Art,
    ));
  }

  public function shop_list()
  {

    // select count article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_count_select.sql";

    $this->aResult["count"] = $this->oDatabase->getSelectDatas($spathSQL, array(
      "id_famille" =>$this->HTML_DATAS["id_famille"],
    ));

    //  is the total number of articles
    $nb_Total_Art = $this->aResult["count"][0]["nbArt"];
    

    // is the number of articles by page
    $nb_Page_Art = 10;

    // Calculate the total number Page and round to the hight value for the pagination
    $nb_Total_Page = $nb_Total_Art / $nb_Page_Art;
    $nb_Total_Page = ceil($nb_Total_Page);

    // is the page number
    $numPage = $this->HTML_DATAS["num_page"];

    // get the n artciles from page n
    $nb_Art = ($numPage - 1) * $nb_Page_Art;

    error_log("nb total page" . $nb_Total_Page);
    $this->aResult["pagination"] = $nb_Total_Page;
    
    // select shop article
    $spathSQL = "../" . $this->Data_ini["PATH_SQL"] . "shop_list_select.sql";

    $this->aResult["shop"] = $this->oDatabase->getSelectDatas($spathSQL, array(
      "nb_Art" => $nb_Art,
      "nb_Page_Art" => $nb_Page_Art,
      "id_famille" =>$this->HTML_DATAS["id_famille"],
    ));
  }
}
