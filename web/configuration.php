<?php
/**
 * /**
 * Class "Configuration" / file "configuration.php"
 *
 *
 *
 * @package piraStack Project
 * @author @Ilyes Bahlagui
 * @copyright  2021
 * @version v1.0
 */
class Configuration {
   


    /**
     * public $ini_array is used to store all datas needed in the file config_piraStack_dev.ini
     * @var array  $ini_array
     * 
     * return all datas in the ini file
     *
     * public static
     */

     public static function  getIniFile(){
        $ini_file='../files/config_pirastack_dev.ini';
        if(is_file($ini_file)){
            return $ini_array=parse_ini_file($ini_file);
        }
        else{
            return false;
        }
     }
}


?>