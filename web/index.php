        
<?php
session_start();
require 'configuration.php';
$Data_ini= Configuration::getIniFile();

// Class dynamic
if ((isset($_GET["page"])) && ($_GET["page"] != "")) {
    $monPHP = $_GET["page"];
} else {
    if ((isset($_POST["page"])) && ($_POST["page"] != "")) {
        $monPHP = $_POST["page"];
    } else {
        $monPHP = "accueil";
    }
}
// Test if classes exist
if (!(file_exists("../". $Data_ini["PATH_CLASS"] . $monPHP . ".php"))) {
    $monPHP = "accueil";
}

$myClass = ucfirst($monPHP);


require "../". $Data_ini["PATH_CLASS"] . $monPHP . ".php";

$oMain = new $myClass();


if ((isset($oMain->HTML_DATAS["bJSON"])) && ($oMain->HTML_DATAS["bJSON"] == 1))	{
	//if AJAX WITH JSON
	$page_to_load= $monPHP . ".html";
} else {
	$page_to_load= "index.html";
}

// load the 'html' page
require "../".$Data_ini["PATH_FILES"] . $page_to_load;

unset($oMain);

?>