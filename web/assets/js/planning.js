
// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 *@param {Array} aPlanningGlobal - array global

*/
var aPlanningGlobal;

// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
    getListGlobalPlanning();
});




// ***********************************************************************************************************************************//
//                                                          getListGlobalPlanning
// ***********************************************************************************************************************************//

// This function select shop with AJAX method
function getListGlobalPlanning() {
    // Object datas
    var datas = {
        bJSON: 1,
        page: "planning_list",

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        // url: "index.php?id=2",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            aPlanningGlobal = result;
            console.log("DATA OK ");
            console.log(aPlanningGlobal);
            generateHtmlPlanning(aPlanningGlobal);
            // generateHtmlCategory(aPlanningGlobal["category"]);
            // iNumPage=1;
            // generateHtmlPagination(parseInt(aPlanningGlobal["pagination"]));


        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });

}

// ***********************************************************************************************************************************//
//                                                          generateHtmlPlanning
// ***********************************************************************************************************************************//

/**
 * @param {Array} aPlanning array parameter
 * @param {String} sHTMLPLANNING global template planning html
 * @param {String} sHTML - template html
 */

// Generate template  Planning HTML
function generateHtmlPlanning(aPlanning) {

    let sHTMLPLANNING = "";
    let sHTML = "";
    

    // This Loop replace "___image___" && "___titre___" && "___horaire_start___"  && "___horaire_end___" with true value
    for (let i = 0; i < aPlanning.length; i++) {

        // html content recovry  with id=template_shop to give sHTML
        sHTML = $('#template_planning').html();

        sHTML = sHTML.replace("___image___", aPlanning[i]["image_url"]);
        sHTML = sHTML.replace("___titre___", aPlanning[i]["titre_planning"]);
        sHTML = sHTML.replace("___horaire_start___", aPlanning[i]["horaire_start"]+"h");
        sHTML = sHTML.replace("___horaire_end___",aPlanning[i]["horaire_end"]+"h");
        
            // Insert the new html content 
        $("#"+aPlanning[i]["nom_jour"]).append(sHTML);
        
    }

    console.log(sHTMLPLANNING);

}