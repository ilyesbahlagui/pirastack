
// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 *@param {Array} aStaffGlobal- array global

*/
var aStaffGlobal;

// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
    getListGlobalStaff();
});




// ***********************************************************************************************************************************//
//                                                          getListGlobalStaff
// ***********************************************************************************************************************************//

// This function select staff with AJAX method
function getListGlobalStaff() {
    // Object datas
    var datas = {
        bJSON: 1,
        page: "staff_list",

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        // url: "index.php?id=2",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            aStaffGlobal = result;
            console.log("DATA OK ");
            console.log(aStaffGlobal);
            generateHtmlStaff(aStaffGlobal);

        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });

}



// ***********************************************************************************************************************************//
//                                                          generateHtmlStaff
// ***********************************************************************************************************************************//

/**
 * @param {Array} aStaffContent array parameter
 * @param {String} sHTMLSTAFF global template staff  staff html
 * @param {String} sHTML - template html
 */

// Generate template  STAFF HTML
function generateHtmlStaff(aStaffContent) {

    let sHTMLSTAFF = "";
    let sHTML = "";

    // This Loop replace "___image___" && "___pseudonyme___" && "___poste___"  && "___description___" with true value
    for (let i = 0; i < aStaffContent.length; i++) {

        // html content recovry  with id=template_staff to give sHTML
        sHTML = $('#template_staff').html();

        sHTML = sHTML.replace("___image___", aStaffContent[i]["image_staff"]);
        sHTML = sHTML.replace("___pseudonyme___", aStaffContent[i]["pseudonyme"]);
        sHTML = sHTML.replace("___poste___", aStaffContent[i]["poste_staff"]);
        sHTML = sHTML.replace("___description___", aStaffContent[i]["description_staff"]);
        
        sHTMLSTAFF += sHTML;
    }
    console.log(sHTMLSTAFF);

    // Insert the new html content 
    $("#template_result_staff").html(sHTMLSTAFF);
}