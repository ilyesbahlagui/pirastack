jQuery(function ($) {

  // Function which adds the 'animated' class to any '.animatable' in view
  var doAnimations = function () {

    // Calc current offset and get all animatables
    var offset = $(window).scrollTop() + $(window).height(),
      $animatables = $('.animatable');

    // Unbind scroll handler if we have no animatables
    if ($animatables.length == 0) {
      $(window).off('scroll', doAnimations);
    }

    // Check all animatables and animate them if necessary
    $animatables.each(function (i) {
      var $animatable = $(this);
      if (($animatable.offset().top + $animatable.height() - 20) < offset) {
        $animatable.removeClass('animatable').addClass('animated');
      }
    });

  };

  // Hook doAnimations on scroll, and trigger a scroll
  $(window).on('scroll', doAnimations);
  $(window).trigger('scroll');

});








// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 *@param {Array} aMapsGlobal- array global

*/
var aMapsGlobal;

// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
  getListGlobalMaps();
});




// ***********************************************************************************************************************************//
//                                                          getListGlobalMaps
// ***********************************************************************************************************************************//

// This function select staff with AJAX method
function getListGlobalMaps() {
  // Object datas
  var datas = {
    bJSON: 1,
    page: "maps_list",

  }

  $.ajax({
    type: "POST",
    url: "index.php",
    data: datas,
    async: false,
    dataType: "json",
    cache: false,
  })
    .done(function (result) {
      aMapsGlobal = result;
      console.log("DATA OK ");
      console.log(aMapsGlobal);
      generateHtmlMaps(aMapsGlobal);

    })
    .fail(function (err) {
      console.log('error : ' + err.status);

      alert("Erreur aucune valeur JSON");

    });

}



// ***********************************************************************************************************************************//
//                                                          generateHtmlMaps
// ***********************************************************************************************************************************//

/**
 * @param {Array} aMapsContent array parameter
 * @param {String} sHTMLMAPS global template MAPS html
 * @param {String} sHTMLMAPS_INFO global template MAPS INFO html
 * @param {String} sHTMLINFO global template  INFO html
 * @param {String} sHTML - template  MAPS html
 * @param {String} sHTML - template MAPS INFO html
 */

// Generate template  MAPS HTML
function generateHtmlMaps(aMapsContent) {

  let sHTMLMAPS = "";
  let sHTMLMAPS_INFO = "";
  let sHTMLINFO = "";
  let sHTML = "";
  let sHTML2 = "";

  // This Loop replace "___image___" && "___titre___" && "___description___" with true value
  for (let i = 0; i < aMapsContent["maps"].length; i++) {

    // html content recovry  with id=template_maps to give sHTML
    sHTML = $('#template_maps').html();

    if (i == 0) {
      sHTML = sHTML.replace("___image___", aMapsContent["maps"][i]["image"]);
      sHTML = sHTML.replace("___titre___", aMapsContent["maps"][i]["titre"]);
      sHTML = sHTML.replace("___description___", aMapsContent["maps"][i]["description"]);
      sHTMLMAPS_INFO = sHTML;

      // This Loop replace "___image___" && "___titre___" && "___description___" with true value

      for (let j = 0; j < aMapsContent["maps_infos"].length; j++) {
        // html content recovry  with id=template_maps to give sHTML
        sHTML2 = $('#template_infos').html();
        sHTML2 = sHTML2.replace("___titre___", aMapsContent["maps_infos"][j]["titre"]);
        sHTML2 = sHTML2.replace("___description___", aMapsContent["maps_infos"][j]["description"]);
        sHTMLINFO += sHTML2;
      }


    }


    else {
      sHTML = sHTML.replace("___image___", aMapsContent["maps"][i]["image"]);
      sHTML = sHTML.replace("___titre___", aMapsContent["maps"][i]["titre"]);
      sHTML = sHTML.replace("___description___", aMapsContent["maps"][i]["description"]);

      sHTMLMAPS += sHTML;
    }

  }
  console.log("infos :" + sHTMLINFO);
  console.log("maps :" + sHTMLMAPS_INFO);
  console.log(sHTMLMAPS);

  // Insert the new html content 
  $("#template_result_maps").html(sHTMLMAPS);
  $("#template_result_maps_infos").append(sHTMLMAPS_INFO);
  $("#template_result_infos").html(sHTMLINFO);


}
