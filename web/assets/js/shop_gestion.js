
// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 *@param {Array} aShopGlobal["shop"] - array global
 *@param {int} iIndiceSave - save the indice array
 *@param {int} iIdSave - save the id product
 *@param {int} iIndiceDelete - save the indice delete 
 *@param {int} iIdDelete - save the id product for delete 

*/
var aShopGlobal;
var iIndiceSave;
var iIdSave;
var iIndiceDelete;
var iIdDelete;
// initialize datatable
var tables;

// ***********************************************************************************************************************************//
//                                                          Ready Function															  //
// ***********************************************************************************************************************************// 

jQuery(function () {
	getShopList();
	// INIT DATATABLE
	tables = $('#table_shop').DataTable(configuration);
	// print name image 
	$("#files").on("change", function () {
		$("#name_file").html($('#files').prop('files')[0].name);
	});

});
// ***********************************************************************************************************************************//
//                                                         CONFIGURATION DATATABLE													  //
// ***********************************************************************************************************************************//
const configuration = {
	"stateSave": false,
	"order": [[1, "asc"]],

	"pagingType": "simple_numbers",
	"searching": true,
	"lengthMenu": [[5, 10, 20, 50, -1], ["5", "10", "20", "50", "Ze total stp"]],
	"language": {
		"info": "Article _START_ à _END_ sur _TOTAL_ sélectionnées",
		"emptyTable": "Aucun utilisateur",
		"lengthMenu": "_MENU_ Article par page",
		"search": "Rechercher : ",
		"zeroRecords": "Aucun article ",
		"paginate": {
			"previous": "Précédent",
			"next": "Suivant"
		},
		"sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
		"sInfoEmpty": "Article(s) 0 sur 0 sélectionnée",
	},
	"columns": [
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": true
		},
		{
			"orderable": false
		},
		{
			"orderable": false
		}
	],
	'retrieve': true
};




// ***********************************************************************************************************************************//
//                                                         ConstructTable
// ***********************************************************************************************************************************//

// Construit mon tableau htm//
function ConstructTable() {
	var sHTML = "<thead>";
	sHTML += "<tr>";
	sHTML += "<td>Nom</td>";
	sHTML += "<td>Prix htt</td>";
	sHTML += "<td>Stock</td>";
	sHTML += "<td class='edit'>Editer</td>";
	sHTML += "<td class='delete'>Supprimer</td>";
	sHTML += "</tr>";
	sHTML += "</thead>";
	sHTML += "<tbody>";
	for (var i = 0; i < aShopGlobal["shop"].length; i++) {
		// sHTML += "<tr>";
		sHTML += '<tr user_id="' + aShopGlobal["shop"][i]["id"] + '">';
		sHTML += "<td>" + aShopGlobal["shop"][i]["nom_produit"] + "</td>";
		sHTML += "<td>" + aShopGlobal["shop"][i]["prix"] + "</td>";
		sHTML += "<td>" + aShopGlobal["shop"][i]["stock"] + "</td>";
		sHTML += "<td class='edit' onClick=\"EditArticle(" + i + "," + aShopGlobal["shop"][i]["id_produit"] + ")\"><i class='fas fa-edit'></i></td>";
		sHTML += "<td class='delete' onClick=\"getDeleteArticle(" + i + "," + aShopGlobal["shop"][i]["id_produit"] + ")\"data-bs-toggle='modal' data-bs-target='#Modal' ><i class='fas fa-trash-alt'></i></td>";
		sHTML += "</tr>";
	}
	sHTML += "</tbody>";
	$('#table_shop').html(sHTML);

}

// ***********************************************************************************************************************************//
// 															 	  AJAX																  //
//                                                       	   getShopList                                                            //
// ***********************************************************************************************************************************// 
// getShopList get global list shop
function getShopList() {
	$('#divModalSaving').show();

	var datas = {
		bJSON: 1,
		page: "shop_gestion_list",

	}
	console.log(datas);

	$.ajax({
		type: "POST",
		url: "index.php",
		data: datas,
		async: false,
		dataType: "json",
		cache: false,
	})
		.done(function (result) {
			// console.log(result);
			console.log(result);
			aShopGlobal = result;
			ConstructTable();
			getTvaDatalist(result["tva"]);
			getCategoryDatalist(result["categorie"]);

			// NewTable();


		})
		.fail(function (err) {
			console.log('error : ' + err.status);

			alert("Erreur aucune valeur JSON");

		});
}


/// ***********************************************************************************************************************************//
//                                                          getNameImage
// ***********************************************************************************************************************************//


/**
 * @param {string} sName - name parameter 
 * @param {string} sNameImage - name image
 *  @return {string} - return name image
 */
// Function construct and return the Name Image 
function getNameImage(sName) {

	sName;//Sweat à capuche

	var sNameImage = RemoveAccent(sName);
	// sNameImage="Sweat a capuche"

	sNameImage = sNameImage.replace(/ /g, "-");
	// sNameImage="Sweat-a-capuche"

	sNameImage = sNameImage.toLowerCase();
	// sNameImage="sweat-a-capuche"


	sNameImage = 'src="assets/image/' + sNameImage + '.jpg"';
	// sNameImage="<img class="main_img" src="assets/image/Sweat a capuche.jpg" alt="" ></img>'



	// console.log(sNameImage);

	return sNameImage;
	// return the value

}

/// ***********************************************************************************************************************************//
//                                                          RemoveAccent
// ***********************************************************************************************************************************//
/**
 * @param {parameter} str 
 * @param {array} aAccent - content the letter all accent 
 * @param {array} aNoAccent - content the letter with not accent 
 * @returns 
 */
function RemoveAccent(str) {
	// letter with accent UTF-8
	var aAccent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	// letter with not accent
	var aNoAccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
	for (var i = 0; i < aAccent.length; i++) {
		str = str.replace(aAccent[i], aNoAccent[i]);
	}
	// return the value
	return str;


}
/// ***********************************************************************************************************************************//
//                                                          NewTable
// ***********************************************************************************************************************************//

//destruct and construct the datatable 
function NewTable() {
	// console.log("table ok");
	// console.log(tables);
	tables.clear();
	tables.destroy();
	ConstructTable();
	tables = $('#table_shop').DataTable(configuration);
}



// ***********************************************************************************************************************************//
//                                                          Add_EditShop                                                             //
// ***********************************************************************************************************************************// 
/**
 * @param {boolean} bInputControl boolean value
 */
function Add_EditShop(AddorEdit) {

	let iIndice;
	// Case or user click on btn modifier or ajouter
	switch (AddorEdit) {
		// case click on modifier so the iIndice is the indice position or article in the array
		case "edit":
			alert("je suis dans modifer " + iIndiceSave);

			iIndice = iIndiceSave;
			break;
		// case click on ajouter so add the new article in the array 
		case 'add':
			iIndice = aShopGlobal["shop"].length;
			alert("je suis dans le add " + iIndice);

			break;
	}

	let bInputControl = ControlRegex();
	// the control input is good can insert the value in the array 
	if (bInputControl) {



		aShopGlobal["shop"][iIndice] = [];
		aShopGlobal["shop"][iIndice]["prix"] = $('#prix').val();
		aShopGlobal["shop"][iIndice]["nom_produit"] = $('#nom_produit').val();
		// aShopGlobal["shop"][iIndice]["nom_categorie"] = aShopGlobal["categorie"][$('#list_categorie').val()]["nom_famille"];
		aShopGlobal["shop"][iIndice]["id_famille_fk"] = $('#list_categorie').val();
		aShopGlobal["shop"][iIndice]["id_tva_fk"] = $('#list_tva').val();
		aShopGlobal["shop"][iIndice]["stock"] = $('#stock').val();
		aShopGlobal["shop"][iIndice]["description"] = $('#description').val();
		// construct the new datatable
		NewTable();
		console.log(aShopGlobal["shop"])
		// empty the input
		EmptyData();

		console.log("control ok ");

	}
	// can't insert the new value in the array  
	else {
		console.log("control not ok  ");
	}
	// iIndiceSave="";
}

// ***********************************************************************************************************************************//
//                                                          EditArticle                                                             //
// ***********************************************************************************************************************************// 


// the function return value and hide or show btn 
function EditArticle(iIndiceEdition, iIdArticle) {
	alert(iIndiceEdition + " et " + iIdArticle);
	iIndiceSave = iIndiceEdition;
	iIdSave = iIdArticle;
	// With the indiceEdition can return value in input 
	$('#nom_produit').val(aShopGlobal["shop"][iIndiceSave]["nom_produit"]);
	$('#list_categorie').val(aShopGlobal["shop"][iIndiceSave]["id_famille_fk"]);
	$('#list_tva').val(aShopGlobal["shop"][iIndiceSave]["id_tva_fk"]);
	$('#prix').val(aShopGlobal["shop"][iIndiceSave]["prix"]);
	$('#stock').val(aShopGlobal["shop"][iIndiceSave]["stock"]);
	$('#description').val(aShopGlobal["shop"][iIndiceSave]["description"]);
	$('.img_print').html('<img class="img_product"' + getNameImage(aShopGlobal["shop"][iIndiceSave]["nom_produit"]) + '</img>');
	// print the Button 
	$("#btn_modifier").show();
	$("#block_files").hide();
	$("#btn_annuler").show();
	$("#btn_annuler_modif").show();
	// hide the button
	$("#btn_ajouter").hide();
	console.log(getNameImage(aShopGlobal["shop"][iIndiceSave]["nom_produit"]));

}

// ***********************************************************************************************************************************//
//                                                          getDeleteArticle                                                          //
// ***********************************************************************************************************************************//
// this function get indice and id for delete 
function getDeleteArticle(iIndicedelete, iIddelete) {
	iIndiceDelete = iIndicedelete;
	iIdDelete = iIddelete;
	console.log("indice => " + iIndiceDelete + "id sup => " + iIdDelete);
}

// ***********************************************************************************************************************************//
//                                                      DeleteArticle                                                                 //
// ***********************************************************************************************************************************//
// delete  the product in tne database and in the array then destruct and construct the datatable
function DeleteArticle() {
	var datas = {
		bJSON: 1,
		page: "shop_gestion_delete",
		id_produit: iIdDelete,
		nom_produit: aShopGlobal["shop"][iIndiceDelete]["nom_produit"],


	}
	console.log(datas);

	$.ajax({
		type: "POST",
		url: "index.php",
		data: datas,
		async: false,
		dataType: "json",
		cache: false,
	})
		.done(function (result) {
			// console.log(result);
			console.log(result);
			if(result==1){
				aShopGlobal["shop"].splice(iIndiceDelete, 1);
				console.log(iIndiceDelete);
				console.log(aShopGlobal["shop"]);
				NewTable();
				EmptyData();
			}
			else{
				alert("impossible de supprimer");
			}
			


		})
		.fail(function (err) {
			console.log('error : ' + err.status);

			alert("Erreur aucune valeur JSON");

		});
}



// ***********************************************************************************************************************************//
//                                                          getTvaDatalist                                                             //
// ***********************************************************************************************************************************// 
/**
 * @param {Array} aDatalist array parameter
 * @param {String} sHTMLSHOP global template tva html
 * @param {String} sHTML - template html
 */

function getTvaDatalist(aDatalist) {
	let sHtmlGlobal = "";
	let sHtml = "";
	// This Loop replace "___id_tva___" && "___name_tva___" with true value

	for (let i = 0; i < aDatalist.length; i++) {
		// html content recovry  with id=list_tva to give sHTML
		sHtml = $("#list_tva").html();
		sHtml = sHtml.replace("___name_tva___", aDatalist[i]["taux_tva"] * 100);
		sHtml = sHtml.replace("___id_tva___", aDatalist[i]["id_tva"]);
		sHtmlGlobal += sHtml;
	}
	// Insert the new html content 

	$("#list_tva").html(sHtmlGlobal);
}
// ***********************************************************************************************************************************//
//                                                          getCategoryDatalist                                                        //
// ***********************************************************************************************************************************// 
/**
 * @param {Array} aDatalist array parameter
 * @param {String} sHTMLSHOP global template categorie html
 * @param {String} sHTML - template html
 */

function getCategoryDatalist(aDatalist) {
	let sHtmlGlobal = "";
	let sHtml = "";
	// This Loop replace "___id_categorie___" && "___name_categorie___" with true value

	for (let i = 0; i < aDatalist.length; i++) {
		// html content recovry  with id=list_categorie to give sHTML
		sHtml = $("#list_categorie").html();
		sHtml = sHtml.replace("___name_categorie___", aDatalist[i]["nom_famille"]);
		sHtml = sHtml.replace("___id_categorie___", aDatalist[i]["id_famille"]);
		sHtmlGlobal += sHtml;
	}
	// Insert the new html content 

	$("#list_categorie").html(sHtmlGlobal);
}

// ***********************************************************************************************************************************//
//                                                          EmptyData                                                               //
// ***********************************************************************************************************************************// 

//function empty the input and element html
function EmptyData() {
	$('#nom_produit').val("");
	$('#list_categorie').val("");
	$('#list_tva').val("");
	$('#nom_produit').val("");
	$('#prix').val("");
	$('#stock').val("");
	$('#description').val("");
	$('#files').val("");
	$('#name_file').html("");
	$('.img_print').html("");


}
// ***********************************************************************************************************************************//
//                                                          ControlRegex                                                               //
// ***********************************************************************************************************************************// 
// ControlRegex controle the value and return true or false 
function ControlRegex() {


	// tests with object RegExp
	// Name product regex
	let oWordReg = new RegExp(/^([a-zA-Z-.|\s]{0,20})$/, 'g');
	let sName = $('#nom_produit').val();
	let bWord = oWordReg.test(sName);
	// Price regex
	let oPriceReg = new RegExp(/^[0-9]{0,5}$/, 'g');
	let iPrice = $('#prix').val();
	let bPrice = oPriceReg.test(iPrice);
	// Stock regex
	let oStockReg = new RegExp(/^[0-9]{0,4}$/, 'g');
	let iStock = $('#stock').val()
	let bStock = oStockReg.test(iStock);
	// Description regex
	let oDescriptionReg = new RegExp(/^[a-zA-Z0-9().,?!|\s]{0,500}$/, 'g');
	let sDescription = $('#description').val();
	let bDescription = oDescriptionReg.test(sDescription);
	// Categorie regex
	let oCatReg = new RegExp(/^([0-9]{1,2})$/, 'g');
	let iCat = $('#list_categorie').val();
	let bCat = oCatReg.test(iCat);
	// TVA regex
	let oTvaReg = new RegExp(/^([0-9]{1,2})$/, 'g');
	let iTva = $('#list_tva').val()
	bTva = oTvaReg.test(iTva);
	// boolean and Value condition
	let bRegex = (bTva && bCat && bDescription && bStock && bPrice && bWord);
	let bValue = (iTva && iCat && sDescription && iStock && iPrice && sName);

	console.log(bTva, bCat, bDescription, bStock, bPrice, bWord);

	// if the boolean is true and value is != "" and null and remove class input-error
	if (bRegex && (bValue != "" && bValue != null)) {
		$("input,textarea").removeClass("input-error")
		return true
	}
	// regex or value is false return false and addClass input-error
	else {
		$("input,textarea").addClass("input-error");
		alert("Vérifier votre saisie");
		return false;
	}

}

// ***********************************************************************************************************************************//
//                                                          CancelAction                                                               //
// ***********************************************************************************************************************************// 
// this function call EmptyData for empty value and hide et show btn
function CancelAction() {
	EmptyData();
	$("#btn_modifier").hide();
	$("#block_files").show();
	$("#btn_annuler").hide();
	$("#btn_ajouter").show();
}
// ***********************************************************************************************************************************//
//                                                          InsertArticle                                                               //
// ***********************************************************************************************************************************// 
/**
 * @param {boolean} bInputControl boolean value
 */


function InsertArticle() {

	// call function for control value with regex
	let bInputControl = ControlRegex();
	// the control input is good can insert the value in the array 
	if (bInputControl) {

		console.log($('#files').prop('files')[0]);

		var datas = new FormData();
		datas.append("bJSON", 1);
		datas.append("page", "shop_gestion_insert");
		datas.append("nom_produit", $('#nom_produit').val());
		datas.append("files", $('#files').prop('files')[0]);
		datas.append("prix", $('#prix').val());
		datas.append("stock", $('#stock').val());
		datas.append("description", $('#description').val());
		datas.append("id_tva", $('#list_tva').val());
		datas.append("id_categorie", $('#list_categorie').val());


		console.log(datas);
		// if is not null insert file image so call ajax 
		if ($('#files').prop('files')[0] != null) {


			$.ajax({
				type: "POST",
				url: "index.php",
				enctype: 'multipart/form-data',
				data: datas,
				async: false,
				processData: false,
				contentType: false,
				dataType: "json",
				cache: false,
			})
				.done(function (result) {
					// console.log(result);
					console.log(result);

					// if the insert is good so insert the new value in the array js 
					if (result["error"] == 0 && result["upload"] == 1) {
						let iIndice;
						iIndice = aShopGlobal["shop"].length;

						aShopGlobal["shop"][iIndice] = [];
						// insert the last id insertion 
						aShopGlobal["shop"][iIndice]["id_produit"] = result['id'];
						aShopGlobal["shop"][iIndice]["prix"] = $('#prix').val();
						aShopGlobal["shop"][iIndice]["nom_produit"] = $('#nom_produit').val();
						aShopGlobal["shop"][iIndice]["id_famille_fk"] = $('#list_categorie').val();
						aShopGlobal["shop"][iIndice]["id_tva_fk"] = $('#list_tva').val();
						aShopGlobal["shop"][iIndice]["stock"] = $('#stock').val();
						aShopGlobal["shop"][iIndice]["description"] = $('#description').val();
						// construct the new datatable
						NewTable();
						console.log(aShopGlobal["shop"])
						// empty the input
						EmptyData();
						console.log("control ok ");
						alert("Ajout du produit est fait !");
					}
					// else can't insert in the array 
					else if (result["error"] == 1) {
						console.log("le controle non ok");
					}
					else {
						switch (result["upload"]) {
							case 3:
								$("#name_file").html("la taille est trop grande");
								break;
							case 0:
								$("#name_file").html("Impossible de télécharger l'image");
								break;
							case 2:
								$("#name_file").html("le type de ficher n'est pas bon");
								break;
							case null:
								$("#name_file").html("Erreur produite durant l'insertion");
								break;
						}
					}
				})
				.fail(function (err) {
					console.log('error : ' + err.status);
					alert("Erreur aucune valeur JSON");
				});
		}
		// else print insert image 
		else {
			$("#name_file").html("Insérer une image");
		}
	}

}

// ***********************************************************************************************************************************//
//                                                          UpdateArticle                                                               //
// ***********************************************************************************************************************************// 
/**
 * @param {boolean} bInputControl boolean value
 */
// function update value 
function UpdateArticle() {

	// call function for control value with regex
	let bInputControl = ControlRegex();
	// the control input is good can insert the value in the array 
	if (bInputControl) {

		var datas = {
			bJSON: 1,
			page: "shop_gestion_update",
			id_produit: iIdSave,
			nom_produit: $('#nom_produit').val(),
			// rename file image in php
			nom_produit_before:aShopGlobal["shop"][iIndiceSave]["nom_produit"],
			prix: $('#prix').val(),
			stock: $('#stock').val(),
			description: $('#description').val(),
			id_tva: $('#list_tva').val(),
			id_categorie: $('#list_categorie').val(),
		}
		console.log(datas);
		$.ajax({
			type: "POST",
			url: "index.php",
			data: datas,
			async: false,
			dataType: "json",
			cache: false,
		})
			.done(function (result) {
				// console.log(result);
				console.log(result);

				// if the insert is good so insert the new value in the array js 
				if (result["error"] == 0) {
					let iIndice;
					iIndice = iIndiceSave;
					console.log("voici l'indice");
					console.log(iIndice);
					aShopGlobal["shop"][iIndice] = [];
					aShopGlobal["shop"][iIndice]["prix"] = $('#prix').val();
					aShopGlobal["shop"][iIndice]["id_produit"] = iIdSave;
					aShopGlobal["shop"][iIndice]["nom_produit"] = $('#nom_produit').val();
					aShopGlobal["shop"][iIndice]["id_famille_fk"] = $('#list_categorie').val();
					aShopGlobal["shop"][iIndice]["id_tva_fk"] = $('#list_tva').val();
					aShopGlobal["shop"][iIndice]["stock"] = $('#stock').val();
					aShopGlobal["shop"][iIndice]["description"] = $('#description').val();
					// construct the new datatable
					NewTable();
					console.log(aShopGlobal["shop"])
					// empty the input
					EmptyData();

					console.log("control ok ");
					alert("Modification du produit est ok !");

				}
				// else can't insert in the array 
				else {
					console.log("le controle non ok");
				}
			})
			.fail(function (err) {
				console.log('error : ' + err.status);
				alert("Erreur aucune valeur JSON");
			});
	}
}

// ***********************************************************//
//                           insertfile                       //
// ***********************************************************//
function fileArticle() {

	// trouver commentaire et faire le controle du fichier coté back en php 
	console.log($('#files').prop('files')[0]);

	file_data = new FormData();
	file_data.append("files", $('#files').prop('files')[0]);
	file_data.append("bJSON", 1);
	file_data.append("name_page", "Sweat");
	file_data.append("page", "shop_gestion_file");
	console.log(file_data);

	$.ajax({
		type: "POST",
		url: "index.php",
		enctype: 'multipart/form-data',
		data: file_data,
		async: false,
		processData: false,
		contentType: false,
		dataType: "json",
		cache: false,
	})
		.done(function (result) {
			// console.log(result);
			console.log(result);



		})
		.fail(function (err) {
			console.log('error : ' + err.status);

			alert("Erreur aucune valeur JSON");

		});

}


