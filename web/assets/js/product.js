// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
    getListProduct();

});

// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 * @param {Array} aProductGlobal - array global
 */

var aProductGlobal;


/// ***********************************************************************************************************************************//
//                                                              AJAX
//                                                          getListProduct
// ***********************************************************************************************************************************//
function getListProduct() {
    var datas = {
        bJSON: 1,
        page: "product_list"

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            console.log(result);
            aProductGlobal = result;
            generateHtmlProduct(aProductGlobal);
        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });
}

/// ***********************************************************************************************************************************//
//                                                          generateHtmlProduct
// ***********************************************************************************************************************************//
/**
 * 
 * @param {array} aProduct array parameter
 * @param {string} sHTML content the element html <img>
 */
function generateHtmlProduct(aProduct) {
    // construct the html element <img>
    sHTML = getNameImage(aProduct[0]["nom_produit"]);

    // Print the value in Html
    $("#category_name").html(aProduct[0]["nom_categorie"]);
    $("#product_name").html(aProduct[0]["nom_produit"]);
    $("#price").html("Prix : " + getPrice(aProduct[0]["prix_unitaire"], aProduct[0]["taux_tva"]) + " TTC");
    $("#description").html(aProduct[0]["description"]);
    $("#img_name").html(sHTML);

    if (aProduct[0]["stock_produit"] == 0) {
        $(".group_buy").html("");
        $(".group_buy").html("<h2 style='color:red;'>Indisponible</h2>");
    }

    // Generate the name secondary img with number 
    for (let i = 0; i < 4; i++) {
        $("#img_name_" + i).html(getNameImage(aProduct[0]["nom_produit"], i));
    }
    console.log(sHTML)


}

/// ***********************************************************************************************************************************//
//                                                          getNameImage
// ***********************************************************************************************************************************//


/**
 * @param {string} sName - name parameter 
 * @param {string} sNameImage - name image
 *  @return {string} - return name image
 */
// Function construct and return the Name Image 
function getNameImage(sName, iNumImage) {

    var sNameImage =RemoveAccent(sName);
    // sNameImage="Sweat à capuche"

    sNameImage = sNameImage.replace(/ /g, "-");
    // sNameImage="Sweat-à-capuche"

    sNameImage = sNameImage.toLowerCase();
    // sNameImage="sweat-à-capuche"

    // // generate name secondary image secondary with Loop of function generateHtmlProduct 
    //     if (iNumImage != null) {
    //         sNameImage = '<img class="main_img" src="assets/image/' + sNameImage+"_"+iNumImage+'.jpg" alt="">';
    //         // sNameImage="<img class="main_img" src="assets/image/Sweat à capuche_1.jpg" alt="" ></img>'
    //     }
    //     else{
    sNameImage = '<img class="main_img" src="assets/image/' + sNameImage + '.jpg" alt="">';
    // sNameImage="<img class="main_img" src="assets/image/Sweat à capuche.jpg" alt="" ></img>'

    // }


    console.log(sNameImage);

    return sNameImage;
    // return the value

}
/// ***********************************************************************************************************************************//
//                                                          RemoveAccent
// ***********************************************************************************************************************************//
/**
 * @param {parameter} str 
 * @param {array} aAccent - content the letter all accent 
 * @param {array} aNoAccent - content the letter with not accent 
 * @returns 
 */
 function RemoveAccent(str) {
	// letter with accent UTF-8
	var aAccent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	// letter with not accent
	var aNoAccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
	for (var i = 0; i < aAccent.length; i++) {
		str = str.replace(aAccent[i], aNoAccent[i]);
	}
	// return the value
	return str;


}

/// ***********************************************************************************************************************************//
//                                                          getPrice
// ***********************************************************************************************************************************//
/**
 * @param {number} Prix - price HT parameter
 * @param {number} Tva tva parameter
 * @param {number} iPrix - price
 * @param {number} iTva - tva
 * @param {number} iPrixTotal -price with tva include
 * 
 * @returns {number} - Price 
 * 
 */
// Function calculate and return the price with tva include
function getPrice(Prix, Tva) {
    var iPrix = parseFloat(Prix);
    var iTva = parseFloat(Tva);
    // calculate the price with tva 
    let iPrixTotal = iPrix + (iPrix * iTva);
    // console.log("prix",iPrixTotal);
    // return the value
    return iPrixTotal;

}


/// ***********************************************************************************************************************************//
//                                                          addProduct
// ***********************************************************************************************************************************//


function addProduct() {




    // if the amount product != 0 
    if (parseInt($("#nb_product").val()) != 0 && parseInt($("#nb_product").val()) != null) {
        $('.btn_buy').hide();
        $('.loader_main').show();
        var datas = {
            bJSON: 1,
            page: "panier_add",
            id_produit: aProductGlobal[0]["id_produit"],
            quantity: parseInt($("#nb_product").val())
        }

        $.ajax({
            type: "POST",
            url: "index.php",
            data: datas,
            async: false,
            dataType: "json",
            cache: false,
        })
            .done(function (result) {
                $('.loader_main').hide();
                $('.btn_buy').show();
                console.log(result);


            })
            .fail(function (err) {
                console.log('error : ' + err.status);

                alert("Erreur aucune valeur JSON");

            });
    }


    else {
        alert("Aucune quantitée selectionner")
    }
}