$(document).ready(function () {
    getListActu();

});
// This function select actu with AJAX method
function getListActu() {
    // Object data
    var datas = {
        bJSON: 1,
        page: "actu_list"

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            console.log("DATA OK ");
            generateHTML(result);


        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });

}
/**
 * 
 * @param {Array} aNewsContent 
 * @param {String} sHTMLGLOBAL
 * @param {String} sHTML
 */

// Generate template HTML
function generateHTML(aNewsContent) {

    var sHTMLGLOBAL = "";
    var sHTML = "";

    // This Loop replace "___url_media___" && "___description___" && "___id_content___" && "___id_type_contenu___" && "___style___" with true value
    for (let i = 0; i < aNewsContent.length; i++) {

        // html content recovry  with id=div_template_actu to give sHTML
        sHTML = $('#div_template_actu').html();

        sHTML = sHTML.replace("___id_content___", aNewsContent[i]["id_contenu"]);
        sHTML = sHTML.replace("___id_type_contenu___", aNewsContent[i]["id_type_contenu_fk"]);

        // if it's a youtube video 
        if (aNewsContent[i]["reseau_social"] == "youtube") {
            // if it's a video so replace ___url_media___ with the url video
            if (aNewsContent[i]["image"] == "" && aNewsContent[i]["video"] !== "") {
                sHTML = sHTML.replace("___url_media___", '<iframe  height="315" src="https://www.youtube.com/embed/' + aNewsContent[i]["video"] + '" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                sHTML = sHTML.replace("___description___", '<img src="./assets/image/youtube-brands.svg" alt=""></img>' + aNewsContent[i]["description"]);
            }
            // if not video so replace ___style___ for the block is none
            else {
                sHTML = "";

            }
        }

        // if it's a twitch video 
        else if (aNewsContent[i]["reseau_social"] == "twitch") {
            // if it's a video so replace ___url_media___ with the url video
            if (aNewsContent[i]["image"] == "" && aNewsContent[i]["video"] !== "") {
                sHTML = sHTML.replace("___url_media___", '  <iframe src="https://player.twitch.tv/?video=' + aNewsContent[i]["video"] + '&parent=localhost&autoplay=false"height="320"allowfullscreen="true"></iframe>');
                sHTML = sHTML.replace("___description___", '<img src="./assets/image/twitch-brands.svg" alt="">' + aNewsContent[i]["description"]);
            }
            // if not video so replace ___style___ for the block is none
            else {
                sHTML = "";
            }
        }

        else {
            if (aNewsContent[i]["image"] == "" && aNewsContent[i]["video"] !== "") {
                sHTML = sHTML.replace("___url_media___", '  <iframe src="https://player.twitch.tv/?video=' + aNewsContent[i]["video"] + '&parent=localhost&autoplay=false"height="320"allowfullscreen="true"></iframe>');
                sHTML = sHTML.replace("___description___", '<img src="./assets/image/twitch-brands.svg" alt="">' + aNewsContent[i]["description"]);
                console.log("twitch" + sHTML);
            }
            // if not video so replace ___style___ for the block is none
            else {
                sHTML = "";
            }

        }





        sHTMLGLOBAL += sHTML;
    }
    // Insert the new html content 
    $("#template_result").html(sHTMLGLOBAL);
}