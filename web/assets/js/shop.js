// Pour moi la variable iNumpage elle se met a jour constament a chaque interaction de l'utilisateur 
// elle se met a jour afin de changer le css et avoir la bonne valeur pour le select
// la function generate shop global elle me genere les articles et affiche les 10 premiers a la page 1 et iNumpage est par defaut mit a 1 
// la function filter categorie recupere les valeurs qui soint true ou false puis je genere une liste avec les id true
// lorsque j'utilise la pagination y a 3 functions:
// la function getPagination => change le css et recupere la valeur du numero selectionner grace au this ou avec l'id  et lance la fonction ajax ListSHop avec le iNumPage a jour
// la function slide permet avec les fleches de naviguer entre les pages il recupere le numero de page il ajoute et reduit de 1 et creer l'id pour l'element this exemple => #1
// puis appel get Pagination et lui transmets le this c'est a dire l'id et le num Page a jour pour mettre a jour le visuel et lancer l'ajax









// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 *@param {Array} aShopGlobal - array global
 *@param {int} iNumPage - number page selected and the value is constent evalution
 *@param {string} sThis - content the element selected or id element selected
 *@param {string} id_list - ID list content value of the array aId
 *@param {int} iPagination is the number of total page
*/
var aShopGlobal;
var iNumPage;
var sThis;
var id_list = "";
var iPagination;
// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
    getListGlobalShop();
});




// ***********************************************************************************************************************************//
//                                                          getListGlobalShop
// ***********************************************************************************************************************************//

// This function select shop with AJAX method
function getListGlobalShop() {
    // Object datas
    var datas = {
        bJSON: 1,
        page: "shop_global_list",
        num_page: 1,

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        // url: "index.php?id=2",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            aShopGlobal = result;
            console.log("DATA OK ");
            console.log(aShopGlobal);
            generateHtmlShop(aShopGlobal["shop"]);
            generateHtmlCategory(aShopGlobal["category"]);
            iNumPage=1;
            generateHtmlPagination(parseInt(aShopGlobal["pagination"]));


        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });

}

// ***********************************************************************************************************************************//
//                                                          generateHtmlShop
// ***********************************************************************************************************************************//

/**
 * @param {Array} aShopContent array parameter
 * @param {String} sHTMLSHOP global template shop html
 * @param {String} sHTML - template html
 */

// Generate template  SHOP HTML
function generateHtmlShop(aShopContent) {

    let sHTMLSHOP = "";
    let sHTML = "";

    // This Loop replace "___lien_produit___" && "___image_produit___" && "___nom_produit___"  && "___prix___" with true value
    for (let i = 0; i < aShopContent.length; i++) {

        // html content recovry  with id=template_shop to give sHTML
        sHTML = $('#template_shop').html();

        sHTML = sHTML.replace("___lien_produit___", "index.php?page=product&id_produit=" + aShopContent[i]["id_produit"]);
        sHTML = sHTML.replace("___image_produit___", getNameImage(aShopContent[i]["nom_produit"]));
        sHTML = sHTML.replace("___nom_produit___", aShopContent[i]["nom_produit"]);
        sHTML = sHTML.replace("___prix___", getPrice(aShopContent[i]["prix_unitaire"], aShopContent[i]["taux_tva"]));
        if( parseInt(aShopContent[i]["stock_produit"])==0){
            sHTML = sHTML.replace(aShopContent[i]["nom_produit"], aShopContent[i]["nom_produit"]+"<br> Non disponible");
        }
        sHTMLSHOP += sHTML;
    }
    // console.log(sHTMLSHOP);

    // Insert the new html content 
    $("#template_result_shop").html(sHTMLSHOP);
}
/// ***********************************************************************************************************************************//
//                                                          generateHtmlCategory
// ***********************************************************************************************************************************//

/**
 * @param {Array} aNewsCategory array parameter
 * @param {String} sHTMLCATEGORY global template category html
 * @param {String} sHTML - template html
 */
function generateHtmlCategory(aNewsCategory) {

    let sHTMLCATEGORY = "";
    let sHTML = "";

    // This Loop replace "___name_famille___" && "___id_category___" with true value
    for (let i = 0; i < aNewsCategory.length; i++) {
        // html content recovry  with id=template_category to give sHTML
        sHTML = $("#template_category").html();
        sHTML = sHTML.replace(/___name_famille___/g, aNewsCategory[i]["nom_famille"]);
        sHTML = sHTML.replace(/___id_category___/g, aNewsCategory[i]["id_famille"]);
        sHTMLCATEGORY += sHTML;

    }
    // console.log(sHTMLCATEGORY);
    $("#template_result_category").html(sHTMLCATEGORY);


}

/// ***********************************************************************************************************************************//
//                                                          getNameImage
// ***********************************************************************************************************************************//


/**
 * @param {string} sName - name parameter 
 * @param {string} sNameImage - name image
 *  @return {string} - return name image
 */
// Function construct and return the Name Image 
function getNameImage(sName) {

    sName=RemoveAccent(sName);
    var sNameImage = "assets/image/" + sName + ".jpg";
    // sNameImage="assets/image/Sweat à capuche.jpg"

    sNameImage = sNameImage.replace(/ /g, "-");
    // sNameImage="assets/image/Sweat-à-capuche.jpg"

    sNameImage = sNameImage.toLowerCase();
    // sNameImage="assets/image/sweat-à-capuche.jpg"

    return sNameImage;
    // return the value
    
}

/// ***********************************************************************************************************************************//
//                                                          RemoveAccent
// ***********************************************************************************************************************************//
/**
 * @param {parameter} str 
 * @param {array} aAccent - content the letter all accent 
 * @param {array} aNoAccent - content the letter with not accent 
 * @returns 
 */
 function RemoveAccent(str) {
	// letter with accent UTF-8
	var aAccent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	// letter with not accent
	var aNoAccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
	for (var i = 0; i < aAccent.length; i++) {
		str = str.replace(aAccent[i], aNoAccent[i]);
	}
	// return the value
	return str;


}
/// ***********************************************************************************************************************************//
//                                                          getPrice
// ***********************************************************************************************************************************//
/**
 * @param {number} Prix - price HT parameter
 * @param {number} Tva tva parameter
 * @param {number} iPrix - price
 * @param {number} iTva - tva
 * @param {number} iPrixTotal -price with tva include
 * 
 * @returns {number} - Price 
 * 
 */
// Function calculate and return the price with tva include
function getPrice(Prix, Tva) {
    var iPrix = parseFloat(Prix);
    var iTva = parseFloat(Tva);
    // calculate the price with tva 
    let iPrixTotal = iPrix + (iPrix * iTva);
    // console.log("prix",iPrixTotal);
    // return the value
    return iPrixTotal;

}


/// ***********************************************************************************************************************************//
//                                                          FilterCategory
// ***********************************************************************************************************************************//

/**
 * @param {Array} aShop - it's the new array content aShopGlobal["category"]
 * @param {Array} aCheckBox - it's the new array with value true or false
 * @param {Array} aNewShop - it's the new array with filter element 
 * @param {Array} aId - it's the new array content id element where value is true
 * @param {int} iCounter - it's increment counter 
*/
// Function filter the categories and print the filter article 
function FilterCategory() {

    var aShop = aShopGlobal["shop"];
    var aCategory = aShopGlobal["category"];
    var aCheckBox = [];
    var aNewShop = [];
    var iCounter = 0;
    var aId = [];
    id_list = "";

    // This Loop create new index category name with their value and the index id_famille with id value
    for (let i = 0; i < aCategory.length; i++) {
        aCheckBox[i] = [];
        aCheckBox[i][aCategory[i]["nom_famille"].toLowerCase()] = $("#flexCheck" + aCategory[i]["nom_famille"]).is(":checked");
        aCheckBox[i]["id_famille"] = aCategory[i]["id_famille"];
    }
    // console.log(aCheckBox);



    // This Loop create array aId with the index id_n where id value == true
    for (let j = 0; j < aCheckBox.length; j++) {
        // if value == true so insert value into array
        if (aCheckBox[j][aCategory[j]["nom_famille"].toLowerCase()] == true) {
            aId[iCounter] = [];
            aId[iCounter]['"id_' + iCounter + '"'] = aCheckBox[j]["id_famille"];
            iCounter = iCounter + 1;
        }

    }

    // This Loop create id list for the SQL request and the condition "WHERE id in(id_list)"
    for (let k = 0; k < aId.length; k++) {

        if ((k != aId.length - 1) && aId.length != 1) {
            id_list += aId[k]['"id_' + k + '"'] + ",";
        }
        else {
            id_list += aId[k]['"id_' + k + '"'];
        }

    }
    console.log("la liste id : " + id_list);
    iNumPage=1;
    console.log("Voila le inombre qui vaut 1 => "+iNumPage);
    console.log("Generate pagination est lancer=> "+iPagination);
    generateHtmlPagination(iPagination);



}

/// ***********************************************************************************************************************************//
//                                                          generateHtmlPagination
// ***********************************************************************************************************************************//
/**
 * @param {String} sHTMLCATEGORY global template Pagination html
 * @param {String} sAfter is parameter for function getSlidePagination
 * @param {String} sBefore is parameter for function getSlidePagination
 */
function generateHtmlPagination(pagination) {


    let sHtmlPagination = "";
    iPagination = pagination;
    let sAfter = "after";
    let sBefore = "before";
    // iNumPage=1;


    // Start Generate Pagination
    sHtmlPagination += `<a onclick='getSlidePagination("${sBefore}");' class='presc' id='presc' >&lt;</a><br>`;

    // This Loop generate pagination
    for (let i = 0; i < iPagination; i++) {
        if (i == iNumPage-1) {
            sHtmlPagination += '<a onclick="getPagination(this);" class="active"  data-page=' + (i + 1) + ' id=' + (i + 1) + '>' + (i + 1) + '</a><br>';
        }
        else {
            sHtmlPagination += '<a onclick="getPagination(this);" class=""  data-page=' + (i + 1) + ' id=' + (i + 1) + '>' + (i + 1) + '</a><br>';
        }
    }

    sHtmlPagination += `<a onclick="getSlidePagination('${sAfter}')" class="suiv" id='suiv' >&gt;</a>`;
    //  End Generate Pagination 

    console.log(sHtmlPagination);
    // Print the Pagination 
    $(".pagination").html(sHtmlPagination);


}

/// ***********************************************************************************************************************************//
//                                                 Function Slide and Effect of Pagination 
// ***********************************************************************************************************************************//


/**
 * 
 * @param {string} sthis content element html 
 * @param {int} NumPage content the number page selected
 */

function getPagination(sthis, NumPage) {

    // return elements that are direct children of <div class="pagination"> and change css class "active" by "" 
    $(".pagination").children().removeClass("active").addClass("");
    // the Select element change css class by "active"
    $(sthis).addClass("active");

    // transmit element html or id element
    sThis = sthis;

    if (NumPage == null && NumPage != iPagination) {
        iNumPage = sthis.getAttribute("data-page");

    }
    else {
        iNumPage = parseInt(NumPage);
    }
    getListShop();
    // alert(iNumPage)
    // console.log(sThis);
}



/**
 * @param {string} sSlide is parameter for function getSlidePagination
 * @param {int} iNumPageTemp is temporary var content the number page for not changed the true number page if condition is not good 
 */
function getSlidePagination(sSlide) {

    let iNumPageTemp;
    // console.log(sThis);

    if (sSlide == 'before') {
        iNumPageTemp = iNumPage - 1;
        if (iNumPageTemp != 0 && iNumPageTemp!=iPagination) {
            sThis = "#" + iNumPageTemp;
            getPagination(sThis, iNumPageTemp);
            iNumPage = iNumPageTemp;
            console.log(" Before le num page : "+iNumPage)
        }
        else {
            iNumPage = iNumPage;
            console.log(" Before le num page : "+iNumPage)

        }
    }
    else {

        iNumPageTemp = parseInt(iNumPage) + 1;
        if (iNumPageTemp != (iPagination + 1)) {
            sThis = "#" + iNumPageTemp;
            getPagination(sThis, iNumPageTemp);
            iNumPage = iNumPageTemp;
            
            console.log("After le num page : "+iNumPage)

        }
        else {
            iNumPage = iNumPage;
            
            console.log("After le num page : "+iNumPage)
        }
    }


}
/// ***********************************************************************************************************************************//
//                                                             getListShop
// ***********************************************************************************************************************************//

// This function select shop with AJAX method
function getListShop() {
    if (id_list == "") {
        // Object datas
        var datas = {
            bJSON: 1,
            page: "shop_global_list",
            num_page: iNumPage,
        }
    }
    else {
        // Object datas
        var datas = {
            bJSON: 1,
            page: "shop_list",
            num_page: iNumPage,
            id_famille: id_list,
        }

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        // url: "index.php?id=2",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            console.log("la nouvelle pagination"+result["pagination"])
            generateHtmlShop(result["shop"]);
            generateHtmlPagination(parseInt(result["pagination"]))
            console.log("Listshop num page "+iNumPage)
        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });

}

