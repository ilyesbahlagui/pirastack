// ***********************************************************************************************************************************//
//                                                          Ready Function
// ***********************************************************************************************************************************//    

jQuery(function () {
    //listen to any change in the inputs

    // input email
    $("#email").on("change", function () {
        getInput('email');
    });
    // input name
    $("#name").on("change", function () {
        getInput('name');

    });
    // input firstname
    $("#firstname").on("change", function () {
        getInput('firstname');

    });
    // input number
    $("#number").on("change", function () {
        getInput('number');

    });

    // input password
    $("#password1").on("change", function () {
        getInput('password1');

    });
    // input password confirmation
    $("#password2").on("change", function () {
        getInput('password2');

    });
    // input date
    $("#date").on("change", function () {
        getInput('date');

    });

    // input female
    $("#female").on("change", function () {
        GenreValidate();
    });
    // input male
    $("#male").on("change", function () {
        GenreValidate();
    });
    $("#autre").on("change", function () {
        GenreValidate();
    });



});

// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
* @param {Object} aInput array input
*/

var aInput = {
    'email': "",
    'name': "",
    'firstname': "",
    'date': "",
    'number': "",
    'password1': "",
    'password2': "",
}

// ***********************************************************************************************************************************//
//                                                          getInput
// ***********************************************************************************************************************************//  

/**
 * 
 * @param {Parameter} sIdInput content the id input 
 */

function getInput(sIdInput) {
    //The construct RegExp create object
    // var object
    let oPasswordReg;
    let oWordReg;
    let oNumberReg;
    let oPasswordReg2;
    let oEmailReg;
    let oDateReg;

    // Value && Boolean
    let sEmail;
    let bEmail;
    let sName;
    let bName;
    let sFirstName;
    let bFirstName;
    let iNumber;
    let bNumber;
    let iDate;
    let bDate;
    let iPassword;
    let bPassword;
    let iPassword2;
    let bPassword2;
    let bGenre;



    switch (sIdInput) {

        // **************************************************************************** //
        //                                   EMAIL                                    //
        // **************************************************************************** //

        // Case or user use the input email
        case 'email':

            // Email Regex
            oEmailReg = new RegExp(/^\w+([\.-]?\w)+@\w+([\.-]?\w+)+(\.\w{2,3})+$/, 'g');

            // Collect the value
            sEmail = $('#email').val();
            // boolean return true or false
            bEmail = oEmailReg.test(sEmail);

            // if the boolean is true delete the class input-error 
            if (bEmail) {
                $("#email").removeClass("input-error");
                $("#email_small").html("");
            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#email").addClass("input-error");
                $("#email_small").addClass("text-error");
                $("#email_small").html("Email invalide");


            }
            console.log(sEmail);
            console.log(bEmail);
            // insert the value in the object
            aInput.email = bEmail;

            break;

        // **************************************************************************** //
        //                                    NAME                                      //
        // **************************************************************************** //

        // Case or user use the input NAME
        case 'name':

            // Name and FirstName Regex
            oWordReg = new RegExp(/^([a-zA-Z]{0,10})$/, 'g');


            // Collect the value
            sName = $('#name').val();
            // boolean return true or false
            bName = oWordReg.test(sName);

            // if the boolean is true delete the class input-error 
            if (bName && sName != "") {
                $("#name").removeClass("input-error");
                $("#name_small").html("");
            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#name").addClass("input-error");
                $("#name_small").addClass("text-error");
                $("#name_small").html("Nom invalide");
                // the boolean == false but in the if condition he can true and null so the input value is not ok so initialize  value a false for insert in the array 
                bName = false;



            }
            console.log(sName);
            console.log(bName);
            // insert the value in the object
            aInput.name = bName;


            break;

        // **************************************************************************** //
        //                                   FIRSTNAME                                  //
        // **************************************************************************** //

        // Case or user use the input FIRSTNAME
        case 'firstname':

            // Name and FirstName Regex
            oWordReg = new RegExp(/^([a-zA-Z]{0,10})$/, 'g');


            // Collect the value
            sFirstName = $('#firstname').val();
            // boolean return true or false
            bFirstName = oWordReg.test(sFirstName);

            // if the boolean is true delete the class input-error 
            if (bFirstName && sFirstName != "") {
                $("#firstname_small").html("");
                $("#firstname").removeClass("input-error");

            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#firstname").addClass("input-error");
                $("#firstname_small").addClass("text-error");
                $("#firstname_small").html("Prénom invalide");
                // the boolean == false but in the if condition he can true and null so the input value is not ok so initialize  value a false for insert in the array 
                // je le mets a false car il peut etre vrai et vide donc il est visible comme true dans le tab donc faut le mettre a false
                bFirstName = false;


            }
            console.log(sFirstName);
            console.log(bFirstName);
            // insert the value in the object
            aInput.firstname = bFirstName;


            break;

        // **************************************************************************** //
        //                              NUMBER PHONE                                    //
        // **************************************************************************** //

        // Case or user use the input number
        case 'number':

            // Number phone Regex
            // find the template in the number and count the length number 
            // Start need one zero
            // After zero have one number between 1 and 8
            // End have 8 number between 0 and 9
            oNumberReg = new RegExp(/^(0[1-8])?\d{8}$/, 'g');
            // Collect the value
            iNumber = $('#number').val();
            // boolean return true or false
            bNumber = oNumberReg.test(iNumber);

            // if the boolean is true delete the class input-error 
            if (bNumber && bNumber != "") {
                $("#number_small").html("");
                $("#number").removeClass("input-error");

            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#number").addClass("input-error");
                $("#number_small").addClass("text-error");
                $("#number_small").html("Numéro non valide ");
                // the boolean == false but in the if condition he can true and null so the input value is not ok so initialize  value a false for insert in the array 
                // je le mets a false car il peut etre vrai et vide donc il est visible comme true dans le tab donc faut le mettre a false
                bNumber = false;
            }

            console.log(iNumber);
            console.log(bNumber);
            // insert the value in the object
            aInput.number = bNumber;

            break;

        // **************************************************************************** //
        //                                    DATE                                      //
        // **************************************************************************** //

        // Case or user use the input date
        case 'date':

            // if browser is safari or IE so inverse date 
            if (detectIEorSafari()) {
                iDate = inverseDate($('#date').val());
            }
            // else collect the value 
            else {
                iDate = $('#date').val();
            }

            $("#date").removeClass("input-error");

            //The construct RegExp create object who find the date correct
            oDateReg = new RegExp(/^\d{4}-\d{2}-\d{2}$/, 'g');

            // boolean return true or false
            bDate = oDateReg.test(iDate);

            console.log(iDate);
            console.log(bDate);
            // insert the value in the object
            aInput.date = bDate;

            break;

        // **************************************************************************** //
        //                                   PASSWORD                                   //
        // **************************************************************************** //

        // Case or user use the input PASSWORD
        case 'password1':

            // PassWord Regex
            // you need a minimum lowercase letter (? =. *[A-Z])
            // you need a minimum capital letter (?=.*[a-z])
            // you need a minimum one number (?=.*[0-9])
            // you need a minimum special character (?=.*[#?!@$%^&*-])
            // you need a minimum length of 8 
            // content no espace

            oPasswordReg = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%.^&*-])(?!.*\s).{8,}$/, 'g');

            // Collect the value
            iPassword = $('#password1').val();
            // boolean return true or false
            bPassword = oPasswordReg.test(iPassword);

            // if the boolean is true delete the class input-error 
            if (bPassword && iPassword != "") {
                $("#password1_small").html("");
                $("#password1").removeClass("input-error");

            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#password1").addClass("input-error");
                $("#password1_small").addClass("text-error");
                $("#password1_small").html("Le mots de passe doit contenir : <br> Au moins 1 lettre miniscule <br> Au moins 1 lettre majuscule <br> Au moins un chiffre <br> Au moins un caractère special <br> Avoir 8 caractères mimimum <br> Aucun espace");
                // the boolean == false but in the if condition he can true and null so the input value is not ok so initialize  value a false for insert in the array 
                // je le mets a false car il peut etre vrai et vide donc il est visible comme true dans le tab donc faut le mettre a false
                bPassword = false;
            }

            console.log(iPassword);
            console.log(bPassword);
            // insert the value in the object
            aInput.password1 = bPassword;

            break;

        // **************************************************************************** //
        //                           PASSWORD CONFIRMATION                              //
        // **************************************************************************** //

        // Case or user use the input PASSWORD CONFIRMATION
        case 'password2':
            oPasswordReg = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%.^&*-])(?!.*\s).{8,}$/, 'g');
            oPasswordReg2 = new RegExp(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%.^&*-])(?!.*\s).{8,}$/, 'g');
            // Collect the value
            iPassword2 = $('#password2').val();
            iPassword = $('#password1').val();
            // boolean return true or false
            bPassword = oPasswordReg.test(iPassword);
            bPassword2 = oPasswordReg2.test(iPassword2);

            // if the boolean is true delete the class input-error /text-error
            if (bPassword2 && bPassword && iPassword == iPassword2 && iPassword2 != "") {
                $("#password2").removeClass("input-error");
                $("#password2_small").removeClass("text-error");
                $("#password2_small").html("Mots de passe identique");


            }
            // the value is false print message and add class input-error/text-error
            else {
                $("#password2").addClass("input-error");
                $("#password2_small").addClass("text-error");
                $("#password2_small").html("Mots de passe non identiques");
                // the boolean == false but in the if condition he can true and null so the input value is not ok so initialize  value a false for insert in the array 
                // je le mets a false car il peut etre vrai et vide donc il est visible comme true dans le tab donc faut le mettre a false
                iPassword2 = false;

            }
            console.log("mdp 1 " + iPassword);
            console.log("mdp 1 " + bPassword);
            console.log("mdp 2 " + iPassword2);
            console.log("mdp 2 " + bPassword2);
            // insert the value in the object
            aInput.password1 = bPassword;
            aInput.password2 = bPassword2;
            break;

    }


    aInput.genre = GenreValidate();
    console.log("le tab ");
    console.log(aInput);





}







// ***********************************************************************************************************************************//
//                                                          FilterGenre
// ***********************************************************************************************************************************// 


/**
 * @param {boolean} bGenre boolean genre
 * @param {boolean} bMan boolean man
 * @param {boolean} bWoman boolean woman
 * 
 */

function GenreValidate() {
    bMan = $('#male').is(':checked');
    bWoman = $('#female').is(':checked');

    if (bWoman == true && bMan == false) {
        bGenre = 1;
    }
    else if (bWoman == false && bMan == true) {
        bGenre = 0;

    }
    else {
        bGenre = 2;
    }
    console.log(bGenre)
    return bGenre;


}



// ***********************************************************************************************************************************//
//                                                          InverseDate
// ***********************************************************************************************************************************// 


/**
 * @param {array} aDates date array 
 * @param {Parameters} sDate parameter
 * Convert date jj/mm/aaaa into aaaa-mm-jj
 * 
 */

function inverseDate(sDate) {
    let aDates = sDate.split("/");
    return aDates[2] + "-" + aDates[1] + "-" + aDates[0];
}



// ***********************************************************************************************************************************//
//                                                          Detect Browser
// ***********************************************************************************************************************************// 

function detectIEorSafari() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11
        return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+)
        return true;
    }

    var safari = ua.indexOf('Safari/');
    var chrome = ua.indexOf('Chrome/');
    if ((safari > 0) && (chrome == -1)) {
        // Safari
        return true;
    }

    // other browser
    return false;
}




// ***********************************************************************************************************************************//
//                                                          InsertUser
// ***********************************************************************************************************************************// 
/**
 * @param {Boolean} bValidate boolean value
 * @param {string} sIdName content the id name
 */

function ValidateInsertUser() {
    let bValidate = true;
    let sIdName;

    for (let ligne in aInput) {
        console.log("L'input "+ligne+" à la valeur "+aInput[ligne]);
        // if the value is false and is null 
        if (aInput[ligne] == false || aInput[ligne] == "") {
            bValidate = false;
            sIdName = ligne;
            $("#" + sIdName).addClass("input-error");

        }
    }
    // if the bValidate is true so call the function AJAX
    if (bValidate) {
        addInsertUser();
        console.log("la saisie est bonne ");
    }
    else {
        alert("Inscription impossible")
        console.log("la saisie est mauvaise ");
    }

}


// ***********************************************************************************************************************************//
//                                                          addInsertUser                                                             //
// ***********************************************************************************************************************************// 
// addInsertUser is call in the function ValidateInsertUser
function addInsertUser() {
    var datas = {
        bJSON: 1,
        page: "inscription_insert",
        name: $('#name').val(),
        firstname: $('#firstname').val(),
        number: $('#number').val(),
        date: $('#date').val(),
        genre: GenreValidate(),
        email: $('#email').val(),
        password1: $('#password1').val(),
        password2: $('#password2').val(),
    }
    console.log("les valeurs => ");
    console.log(datas);
    $.ajax({
        type: "POST",
        url: "index.php",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            // console.log(result);
            console.log(result);
            // if not error in the back end return 1 
            if (result == 1) {
                alert("inscription réusie")
            }
            // if error in the back end control return the array with the id name  for print message error 
            else {
                for (let i = 0; i < result.length; i++) {
                    $("#" + result[i]).addClass("input-error");
                    $("#" + result[i] + "_small").addClass("text-error");
                    $("#" + result[i] + "_small").html("Invalide");
                }
            }
        })
        .fail(function (err) {
            console.log('error : ' + err.status);
            alert("Erreur aucune valeur JSON");
        });
}
