// ***********************************************************************************************************************************//
//                                                          ready function
// ***********************************************************************************************************************************//
$(document).ready(function () {
    getListPanier();

});
$(window).resize(function () {
    var windowsize = $(window).width();
    if (windowsize < 768) {
        $('.mobile_div').html("<div>Produit</div><div>Prix</div><div>Quantité</div><div>Total</div>");
    } else {
        $('.mobile_div').empty()
    }
});
// ***********************************************************************************************************************************//
//                                                         VAR GLOBALE
// ***********************************************************************************************************************************//
/**
 * @param {Array} aPanierGlobal - array global
 */

var aPanierGlobal;


/// ***********************************************************************************************************************************//
//                                                              AJAX
//                                                          getListPanier
// ***********************************************************************************************************************************//
function getListPanier() {
    var datas = {
        bJSON: 1,
        page: "panier_list"

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            console.log(result);
            aPanierGlobal = result;
            generateHtmlPanier(aPanierGlobal);
        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });
}

/// ***********************************************************************************************************************************//
//                                                          generateHtmlPanier
// ***********************************************************************************************************************************//

/**
 * @param {Array} aPanier array parameter
 * @param {String} sHTMLPANIER global template panier html
 * @param {String} sHTML - template html
 * @param {int} iPrice - price of product
 */

// Generate template  panier HTML
function generateHtmlPanier(aPanier) {

    let sHTMLPANIER = "";
    let sHTML = "";
    let iPrice = 0;

    // aPanier is not null print product
    if (aPanier["article"]!= "" && aPanier["panier"]!= "" ) {
        // This Loop replace "___lien_produit___" && "___image_produit___" && "___nom_produit___"  && "___prix___" with true value
        for (let i = 0; i < aPanier["article"].length; i++) {

            // html content recovry  with id=template_panier to give sHTML
            sHTML = $('#template_panier').html();
            iPrice = getPrice(aPanier["article"][i]["prix_unitaire"], aPanier["article"][i]["taux_tva"]);
            sHTML = sHTML.replace("___image___", getNameImage(aPanier["article"][i]["nom_produit"]));
            sHTML = sHTML.replace("___nom_product___", aPanier["article"][i]["nom_produit"]);
            sHTML = sHTML.replace("___price___", iPrice + " TTC");

            for (let j = 0; j < aPanier["panier"].length; j++) {
                
                if (aPanier["panier"][j]["id_produit"] == aPanier["article"][i]["id_produit"]) {
                    sHTML = sHTML.replace("___quantity___", aPanier["panier"][j]["quantity"]);
                    sHTML = sHTML.replace("___total_price___", iPrice * aPanier["panier"][j]["quantity"] + " TTC");

                }
            }

            sHTML = sHTML.replace("___btn_delete___", '<a onclick="DeletePanier(' + i + "," + aPanier["article"][i]["id_produit"] + ')"><i class="lg_icon fas fa-times fa-2x"></i><i class="sm_icon fas fa-times-circle"></i></a>');
            // <a onclick=""></a><i class="lg_icon fas fa-times fa-2x"></i><i class="sm_icon fas fa-times-circle"></i></a>
            sHTMLPANIER += sHTML;
        }
        console.log(sHTMLPANIER);

        // Insert the new html content 
        $("#template_result_panier").html(sHTMLPANIER);
    }

    // aPanier is null print "Aucun article"
    else {
        // Insert the new html content 
        $("#template_result_panier").html("<div class='text-center mt-2 '><h3>AUCUN ARTICLE</h3></div>");
    }

}
/// ***********************************************************************************************************************************//
//                                                          getNameImage
// ***********************************************************************************************************************************//


/**
 * @param {string} sName - name parameter 
 * @param {string} sNameImage - name image
 *  @return {string} - return name image
 */
// Function construct and return the Name Image 
function getNameImage(sName) {

    var sNameImage = RemoveAccent(sName);
    // sNameImage="Sweat à capuche"

    sNameImage = sNameImage.replace(/ /g, "-");
    // sNameImage="Sweat-à-capuche"

    sNameImage = sNameImage.toLowerCase();
    // sNameImage="sweat-à-capuche"

    sNameImage = "assets/image/" + sNameImage + ".jpg";
    // sNameImage="<img class="main_img" src="assets/image/Sweat à capuche.jpg" alt="" ></img>'

    // }


    console.log(sNameImage);

    return sNameImage;
    // return the value

}
/// ***********************************************************************************************************************************//
//                                                          RemoveAccent
// ***********************************************************************************************************************************//
/**
 * @param {parameter} str 
 * @param {array} aAccent - content the letter all accent 
 * @param {array} aNoAccent - content the letter with not accent 
 * @returns 
 */
 function RemoveAccent(str) {
	// letter with accent UTF-8
	var aAccent = [
		/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g, // C, c
	];
	// letter with not accent
	var aNoAccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
	for (var i = 0; i < aAccent.length; i++) {
		str = str.replace(aAccent[i], aNoAccent[i]);
	}
	// return the value
	return str;


}


/// ***********************************************************************************************************************************//
//                                                          getPrice
// ***********************************************************************************************************************************//
/**
 * @param {number} Prix - price HT parameter
 * @param {number} Tva tva parameter
 * @param {number} iPrix - price
 * @param {number} iTva - tva
 * @param {number} iPrixTotal -price with tva include
 * 
 * @returns {number} - Price 
 * 
 */
// Function calculate and return the price with tva include
function getPrice(Prix, Tva) {
    var iPrix = parseFloat(Prix);
    var iTva = parseFloat(Tva);
    // calculate the price with tva 
    let iPrixTotal = iPrix + (iPrix * iTva);
    // console.log("prix",iPrixTotal);
    // return the value
    return iPrixTotal;

}
/// ***********************************************************************************************************************************//
//                                                          DeletePanier
// ***********************************************************************************************************************************//
function DeletePanier(iIndice, iId) {
    console.log(iIndice, iId)
    // aPanierGlobal["article"].splice(iIndice, 1);



    var datas = {
        bJSON: 1,
        page: "panier_delete",
        iIndice: iIndice,
        id_produit: iId,

    }

    $.ajax({
        type: "POST",
        url: "index.php",
        data: datas,
        async: false,
        dataType: "json",
        cache: false,
    })
        .done(function (result) {
            console.log(result);
            if (result == 1) {
                for (i = 0; i < aPanierGlobal["article"].length; i++) {

                    if (iId == aPanierGlobal["article"][i]["id_produit"]) {
                        Indice_save = i;
                        alert(Indice_save);

                    }
                }

                aPanierGlobal["article"].splice(Indice_save, 1);
                // aPanierGlobal["panier"].splice(Indice_save, 1);
                generateHtmlPanier(aPanierGlobal);

            }
            else {
                // alert("erreur panier")
            }


        })
        .fail(function (err) {
            console.log('error : ' + err.status);

            alert("Erreur aucune valeur JSON");

        });
}