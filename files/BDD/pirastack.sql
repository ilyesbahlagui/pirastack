-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 22 juin 2022 à 12:48
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pirastack`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_etat_commande` int(11) NOT NULL,
  `numero_commande` varchar(11) NOT NULL,
  `date_commande` date NOT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `COMMANDE_UTILISATEUR_FK` (`id_utilisateur`),
  KEY `COMMANDE_ETAT_COMMANDE0_FK` (`id_etat_commande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commande__produit`
--

DROP TABLE IF EXISTS `commande__produit`;
CREATE TABLE IF NOT EXISTS `commande__produit` (
  `id_produit` int(11) NOT NULL,
  `id_commande` int(11) NOT NULL,
  `quantite_commande` int(11) NOT NULL,
  `prix_total` int(11) NOT NULL,
  PRIMARY KEY (`id_produit`,`id_commande`),
  KEY `COMMANDE__PRODUIT_COMMANDE0_FK` (`id_commande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contenu`
--

DROP TABLE IF EXISTS `contenu`;
CREATE TABLE IF NOT EXISTS `contenu` (
  `id_contenu` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_contenu_fk` int(11) NOT NULL,
  `titre_contenu` varchar(100) NOT NULL,
  `description_contenu` varchar(300) NOT NULL,
  `reseau_social` varchar(150) DEFAULT NULL,
  `date_insert_contenu` date NOT NULL,
  `video_url` varchar(2000) DEFAULT NULL,
  `image_url` varchar(300) DEFAULT NULL,
  `index_contenu` int(10) DEFAULT NULL,
  PRIMARY KEY (`id_contenu`),
  KEY `CONTENU_TYPE_CONTENU_FK` (`id_type_contenu_fk`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contenu`
--

INSERT INTO `contenu` (`id_contenu`, `id_type_contenu_fk`, `titre_contenu`, `description_contenu`, `reseau_social`, `date_insert_contenu`, `video_url`, `image_url`, `index_contenu`) VALUES
(1, 1, 'Emission 1', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(2, 1, 'Emission 2', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(3, 2, 'Production 1 ', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(4, 2, 'Production 2', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(5, 3, 'Concepts 1', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(6, 3, 'Concepts 2', 'Lorem ipsum dolor dolor tagahd zekej zeçner', '', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(7, 4, 'Actualités 1', 'Clip du jour Chaîne Youtube', 'youtube', '2021-08-25', 'F26cq6XsNg0', NULL, NULL),
(8, 4, 'Actualités 2', 'Dernière vidéo YouTube Chaîne Bonus\r\n', 'youtube', '2021-08-25', 'ypc-5Hrey_k', NULL, NULL),
(9, 4, 'Actualités 3', 'Clip du jour Chaîne Twitch', 'twitch', '2021-08-25', '1128250949', NULL, NULL),
(10, 4, 'Actualités 4', 'Clip de l’année Chaîne Twitch\r\n', 'twitch', '2021-08-25', '1124166253', NULL, NULL),
(11, 4, 'Actualités 5', '<img src=\"./assets/image/twitter-brands.svg\" alt=\"\">« Vous en pensez quoi si je fait la #ZLAN2021 ?»', 'twitter', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(12, 4, 'Actualités 6', '<img src=\"./assets/image/twitter-brands.svg\" alt=\"\">« Vous en pensez quoi si je fait la #ZLAN2021 ?»', 'twitter', '2021-08-25', NULL, '<img src=\"./assets/image/img_black.PNG\" alt=\"\">', NULL),
(13, 5, 'BOXFIGHT 1Vs1 : PIRASTACK EDITION !', 'The most advanced buildfight map with many pro features. FPS Boost Approved.', NULL, '2021-09-08', NULL, './assets/image/photo_maps_2.svg', NULL),
(14, 5, 'BOXFIGHT 2VS2 : PIRASTACK EDITION !', 'The most advanced buildfight map with many pro features. FPS Boost Approved.', NULL, '2021-09-08', NULL, './assets/image/photo_maps_2.svg', NULL),
(15, 5, 'BOXFIGHT 3VS3 : PIRASTACK EDITION !', 'The most advanced buildfight map with many pro features. FPS Boost Approved.', NULL, '2021-09-08', NULL, './assets/image/photo_maps_2.svg', NULL),
(16, 5, 'BOXFIGHT 2VS4 : PIRASTACK EDITION !', 'The most advanced buildfight map with many pro features. FPS Boost Approved.', NULL, '2021-09-08', NULL, './assets/image/photo_maps_2.svg', NULL),
(17, 7, 'N’oubliez pas le Code Créateur : PirAStack #ad', '<p>Dans la boutique Fortnite et le shop Epic Games sur tous vos achats pour me soutenir !</p>', NULL, '2021-09-08', NULL, NULL, 1),
(18, 7, 'Les prochaines sorties de map :', '<ul><li>Une map Goulag sur le thème de Tilted Towers.</li><li>Une map Echauffement Aim&Edit.</li><li>Une map Free For All pour 16 joueurs.</li></ul>', NULL, '2021-09-08', NULL, NULL, 2),
(19, 7, 'Bientôt :', '<p>Une vidéo de présentation par map (caractéristiques, mises à jours, tutos d’utilisations).</p>', NULL, '2021-09-08', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `etat_commande`
--

DROP TABLE IF EXISTS `etat_commande`;
CREATE TABLE IF NOT EXISTS `etat_commande` (
  `id_etat_commande` int(11) NOT NULL AUTO_INCREMENT,
  `etape_commande` varchar(50) NOT NULL,
  `date_etape_commande` date NOT NULL,
  PRIMARY KEY (`id_etat_commande`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

DROP TABLE IF EXISTS `famille`;
CREATE TABLE IF NOT EXISTS `famille` (
  `id_famille` int(11) NOT NULL AUTO_INCREMENT,
  `nom_famille` varchar(100) NOT NULL,
  `code_famille` varchar(100) NOT NULL,
  PRIMARY KEY (`id_famille`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `famille`
--

INSERT INTO `famille` (`id_famille`, `nom_famille`, `code_famille`) VALUES
(1, 'Vêtement', '1'),
(2, 'Informatique', '2'),
(3, 'Alimentaire', '3'),
(4, 'Jeux', '4');

-- --------------------------------------------------------

--
-- Structure de la table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `id_page` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `nom_page` varchar(1000) NOT NULL,
  `contenu_page` varchar(3000) NOT NULL,
  PRIMARY KEY (`id_page`),
  KEY `PAGE_UTILISATEUR_FK` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `page`
--

INSERT INTO `page` (`id_page`, `id_utilisateur`, `nom_page`, `contenu_page`) VALUES
(1, 2, 'Mentions Légales', 'h1><strong>Mentions légales</strong></h1>\r\n\r\n<article class=\"p-3\">\r\n    <h2>Titre de l’article mention...</h2>\r\n\r\n    <p>Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis. quamquam te quidem video minime esse deterritum.\r\n    Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis.</p>\r\n</article>\r\n\r\n<article class=\"p-3\">\r\n    <h2>Titre de l’article mention...</h2>\r\n\r\n    <p>Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis. quamquam te quidem video minime esse deterritum.\r\n    Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis.</p>\r\n</article>\r\n\r\n<article class=\"p-3\">\r\n    <h2>Titre de l’article mention...</h2>\r\n\r\n    <p>Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis. quamquam te quidem video minime esse deterritum.\r\n    Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis.</p>\r\n</article>\r\n\r\n<article class=\"p-3\">\r\n    <h2>Titre de l’article mention...</h2>\r\n\r\n    <p>Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis. quamquam te quidem video minime esse deterritum.\r\n    Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios a studiis.</p>\r\n</article>');

-- --------------------------------------------------------

--
-- Structure de la table `partenaire`
--

DROP TABLE IF EXISTS `partenaire`;
CREATE TABLE IF NOT EXISTS `partenaire` (
  `id_partenaire` int(11) NOT NULL AUTO_INCREMENT,
  `nom_partenaire` varchar(100) NOT NULL,
  `date_insert_partenaire` date NOT NULL,
  `image_partenaire` varchar(100) NOT NULL,
  PRIMARY KEY (`id_partenaire`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `partenaire`
--

INSERT INTO `partenaire` (`id_partenaire`, `nom_partenaire`, `date_insert_partenaire`, `image_partenaire`) VALUES
(1, 'RedBull', '2021-08-25', './assets/image/redBull-logo.svg'),
(2, 'Youtube', '2021-08-25', './assets/image/youtube-logo.svg'),
(3, 'EA', '2021-08-25', './assets/image/ea-logo.svg'),
(4, 'Monaco', '2021-08-25', './assets/image/monaco-esports-logo.svg');

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id_pays` int(11) NOT NULL AUTO_INCREMENT,
  `nom_pays` varchar(11) NOT NULL,
  PRIMARY KEY (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`id_pays`, `nom_pays`) VALUES
(1, 'France'),
(2, 'Espagne'),
(3, 'Italie');

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

DROP TABLE IF EXISTS `planning`;
CREATE TABLE IF NOT EXISTS `planning` (
  `id_planning` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur_fk` int(11) NOT NULL,
  `nom_jour` varchar(20) NOT NULL,
  `numero_jour` int(11) NOT NULL,
  `titre_planning` varchar(100) NOT NULL,
  `horaire_start` int(6) DEFAULT NULL,
  `horaire_end` int(6) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `date_insert_planning` date NOT NULL,
  PRIMARY KEY (`id_planning`),
  KEY `PLANNING_UTILISATEUR_FK` (`id_utilisateur_fk`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `planning`
--

INSERT INTO `planning` (`id_planning`, `id_utilisateur_fk`, `nom_jour`, `numero_jour`, `titre_planning`, `horaire_start`, `horaire_end`, `image_url`, `date_insert_planning`) VALUES
(1, 1, 'lundi', 2, 'Start\'n\'chill', 21, 22, './assets/image/MM.svg', '2021-09-16'),
(2, 1, 'lundi', 2, 'Prediction', 19, 20, './assets/image/Piracast.svg', '2021-09-16'),
(3, 1, 'lundi', 2, 'Au choix', 21, 22, './assets/image/Startnchill.svg', '2021-09-16'),
(4, 1, 'lundi', 2, 'Start\'n\'chill', 21, 22, './assets/image/Startnchill.svg', '2021-09-16'),
(5, 1, 'mardi', 3, 'Start\'n\'chill', 20, 22, './assets/image/MM.svg', '2021-09-16'),
(6, 1, 'mardi', 3, 'Prediction', 21, 23, './assets/image/arenastack.svg', '2021-09-16'),
(7, 1, 'mardi', 3, 'Au choix', 21, 22, './assets/image/MM.svg', '2021-09-16'),
(8, 1, 'mardi', 3, 'Start\'n\'chill', 21, 22, './assets/image/Startnchill.svg', '2021-09-16'),
(9, 1, 'mercredi', 4, 'Start\'n\'chill', 21, 22, './assets/image/MM.svg', '2021-09-16'),
(10, 1, 'mercredi', 4, 'Prediction', 21, 22, './assets/image/Piracast.svg', '2021-09-16'),
(11, 1, 'mercredi', 4, 'Au choix', 21, 22, './assets/image/MM.svg', '2021-09-16'),
(12, 1, 'mercredi', 4, 'Start\'n\'chill', 8, 11, './assets/image/MM.svg', '2021-09-16'),
(13, 1, 'jeudi', 5, 'Start\'n\'chill', 21, 22, './assets/image/MM.svg', '2021-09-16'),
(14, 1, 'vendredi', 6, 'Prediction', 21, 22, './assets/image/Piracast.svg', '2021-09-16'),
(15, 1, 'samedi', 7, 'Au choix', 14, 15, './assets/image/MM.svg', '2021-09-16'),
(16, 1, 'dimanche', 1, 'Start\'n\'chill', 21, 22, './assets/image/Startnchill.svg', '2021-09-16'),
(17, 1, 'lundi', 2, 'test', 20, 21, './assets/image/MM.svg', '2021-10-07');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id_produit` int(11) NOT NULL AUTO_INCREMENT,
  `id_famille_fk` int(11) NOT NULL,
  `id_tva_fk` int(11) NOT NULL,
  `nom_produit` varchar(100) NOT NULL,
  `prix_unitaire` float NOT NULL,
  `description_produit` varchar(1000) NOT NULL,
  `stock_produit` int(11) NOT NULL,
  PRIMARY KEY (`id_produit`),
  KEY `PRODUIT_FAMILLE_FK` (`id_famille_fk`),
  KEY `PRODUIT_TVA0_FK` (`id_tva_fk`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `id_famille_fk`, `id_tva_fk`, `nom_produit`, `prix_unitaire`, `description_produit`, `stock_produit`) VALUES
(107, 1, 1, 'Sweat à capuche', 25, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere, turpis eget scelerisque sagittis, quam eros elementum purus, in tristique neque erat et leo. Ut lacinia nisi ac venenatis ornare. Phasellus felis est, tincidunt eget tortor sed, efficitur hendrerit magna. Nam nisl augue, accumsan at nisi et, varius porta ipsum. Praesent faucibus sollicitudin condimentum. Fusce quis ipsum dignissim, vulputate nisi at, elementum ipsum. Nam vel urna enim.', 250),
(109, 1, 1, 'Bermuda', 20, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere, turpis eget scelerisque sagittis, quam eros elementum purus, in tristique neque erat et leo. Ut lacinia nisi ac venenatis ornare. Phasellus felis est, tincidunt eget tortor sed, efficitur hendrerit magna. Nam nisl augue, accumsan at nisi et, varius porta ipsum. Praesent faucibus sollicitudin condimentum. Fusce quis ipsum dignissim, vulputate nisi at, elementum ipsum. Nam vel urna enim.', 250),
(110, 1, 1, 'Pantalon', 35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere, turpis eget scelerisque sagittis, quam eros elementum purus, in tristique neque erat et leo. Ut lacinia nisi ac venenatis ornare. Phasellus felis est, tincidunt eget tortor sed, efficitur hendrerit magna. Nam nisl augue, accumsan at nisi et, varius porta ipsum. Praesent faucibus sollicitudin condimentum. Fusce quis ipsum dignissim, vulputate nisi at, elementum ipsum. Nam vel urna enim.', 50),
(111, 2, 4, 'Souris gaming', 60, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere, turpis eget scelerisque sagittis, quam eros elementum purus, in tristique neque erat et leo. Ut lacinia nisi ac venenatis ornare. Phasellus felis est, tincidunt eget tortor sed, efficitur hendrerit magna. Nam nisl augue, accumsan at nisi et, varius porta ipsum. Praesent faucibus sollicitudin condimentum. Fusce quis ipsum dignissim, vulputate nisi at, elementum ipsum. Nam vel urna enim.', 150),
(182, 1, 2, 'T-shirt', 15, 'TTTTJFGSDHFQLSDFHLQSjklqferfg', 500),
(183, 1, 5, 'hgjghf', 14, 'hjgjhgj', 14);

-- --------------------------------------------------------

--
-- Structure de la table `reseau_social`
--

DROP TABLE IF EXISTS `reseau_social`;
CREATE TABLE IF NOT EXISTS `reseau_social` (
  `id_reseau_social` int(11) NOT NULL AUTO_INCREMENT,
  `nom_reseau_social` varchar(50) NOT NULL,
  `nom_profil` varchar(50) NOT NULL,
  `lien_profil` varchar(2000) NOT NULL,
  PRIMARY KEY (`id_reseau_social`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `reseau_social`
--

INSERT INTO `reseau_social` (`id_reseau_social`, `nom_reseau_social`, `nom_profil`, `lien_profil`) VALUES
(11, 'tiktok', 'PirAStack', 'https://vm.tiktok.com/ZMRMHyt3S/'),
(12, 'youtube', 'PirAStack', 'https://www.youtube.com/channel/UC_NFNQF3Ti2wNQqLxP9Q4AA'),
(13, 'twitch', 'PirAStack', 'https://www.twitch.tv/pirastack'),
(14, 'instagram', 'pirastack', 'https://www.instagram.com/pirastack/'),
(15, 'snapchat', 'PirAStack', 'https://story.snapchat.com/@pirastance'),
(16, 'twitter', 'pirAStack', 'https://twitter.com/PirAStack');

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `nom_role` varchar(50) NOT NULL,
  `code_role` int(11) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id_role`, `nom_role`, `code_role`) VALUES
(1, 'utilisateur', 1),
(2, 'Staff', 2),
(3, 'Admin', 3);

-- --------------------------------------------------------

--
-- Structure de la table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `id_staff` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) DEFAULT NULL,
  `nom_staff` varchar(100) NOT NULL,
  `pseudonyme` varchar(50) NOT NULL,
  `poste_staff` varchar(50) NOT NULL,
  `description_staff` varchar(300) NOT NULL,
  `image_staff` varchar(2000) NOT NULL,
  `date_insert_staff` date NOT NULL,
  PRIMARY KEY (`id_staff`),
  KEY `STAFF_UTILISATEUR_FK` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `staff`
--

INSERT INTO `staff` (`id_staff`, `id_utilisateur`, `nom_staff`, `pseudonyme`, `poste_staff`, `description_staff`, `image_staff`, `date_insert_staff`) VALUES
(1, NULL, 'Robert', 'Robertdu13', 'Ingénieur du son', 'Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios.', './assets/image/robert.jpg', '2021-08-25'),
(2, 1, 'lucas', 'lcaax', 'monteur video', 'lorem upsum', './assets/image/lucas.jpg', '2021-10-11'),
(3, NULL, 'Mathieu', 'Matomix', 'Manager', 'Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios.', './assets/image/mathieu.jpg', '2021-08-25'),
(4, 4, 'Margaux', 'Margor', 'Assistante personnelle', 'Haec igitur Epicuri non probo, inquam. De cetero vellem equidem aut ipse doctrinis fuisset instructior est enim, quod tibi ita videri necesse est, non satis politus iis artibus, quas qui tenent, eruditi appellantur aut ne deterruisset alios.', './assets/image/margaux.jpg', '2021-08-25');

-- --------------------------------------------------------

--
-- Structure de la table `tva`
--

DROP TABLE IF EXISTS `tva`;
CREATE TABLE IF NOT EXISTS `tva` (
  `id_tva` int(11) NOT NULL AUTO_INCREMENT,
  `labelle_tva` varchar(50) NOT NULL,
  `taux_tva` float NOT NULL,
  PRIMARY KEY (`id_tva`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tva`
--

INSERT INTO `tva` (`id_tva`, `labelle_tva`, `taux_tva`) VALUES
(1, 'tva_20', 0.2),
(2, 'tva_10', 0.1),
(3, 'tva_8.5', 0.085),
(4, 'tva_5.5', 0.055),
(5, 'tva_2.1', 0.021);

-- --------------------------------------------------------

--
-- Structure de la table `type_contenu`
--

DROP TABLE IF EXISTS `type_contenu`;
CREATE TABLE IF NOT EXISTS `type_contenu` (
  `id_type_contenu` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `titre_type_contenu` varchar(100) NOT NULL,
  `date_insert_titre` date NOT NULL,
  PRIMARY KEY (`id_type_contenu`),
  KEY `TYPE_CONTENU_UTILISATEUR_FK` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_contenu`
--

INSERT INTO `type_contenu` (`id_type_contenu`, `id_utilisateur`, `titre_type_contenu`, `date_insert_titre`) VALUES
(1, 1, 'Emission', '2021-08-25'),
(2, 1, 'Production', '2021-08-25'),
(3, 1, 'Concepts', '2021-08-25'),
(4, 1, 'Actualité', '2021-08-25'),
(5, 1, 'Maps', '2021-08-25'),
(7, 1, 'Information Maps', '2021-09-08');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `id_ville_fk` int(11) DEFAULT NULL,
  `id_role` int(11) NOT NULL,
  `nom_utilisateur` varchar(50) NOT NULL,
  `prenom_utilisateur` varchar(100) NOT NULL,
  `age_utilisateur` int(11) NOT NULL,
  `genre` varchar(1) NOT NULL,
  `email_utilisateur` varchar(50) NOT NULL,
  `numero_utilisateur` int(11) DEFAULT NULL,
  `mdp_utilisateur` varchar(500) NOT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `date_insert_utilisateur` date NOT NULL,
  PRIMARY KEY (`id_utilisateur`),
  KEY `UTILISATEUR_VILLE_FK` (`id_ville_fk`),
  KEY `UTILISATEUR_ROLE0_FK` (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `id_ville_fk`, `id_role`, `nom_utilisateur`, `prenom_utilisateur`, `age_utilisateur`, `genre`, `email_utilisateur`, `numero_utilisateur`, `mdp_utilisateur`, `adresse`, `date_insert_utilisateur`) VALUES
(1, 1, 1, 'React', 'Michael', 20, '1', 'Miccka30@gmail.com', 612345678, 'a123', '13 rue de gantt', '2021-08-25'),
(2, 4, 3, 'Druguet', 'Luc', 35, '1', 'luc@gmail.com', 612345678, 'b123', 'Avenue du marechal', '2021-08-25'),
(3, 2, 1, 'Pablo', 'Kevin', 19, '1', 'Kevin@gmail.com', 612345678, 'c123', 'rue jupiter', '2021-08-25'),
(4, 3, 2, 'Escobar', 'Sony', 22, '1', 'Sony@gmail.com', 612345678, 'd123', 'rue tristan', '2021-08-25'),
(6, NULL, 1, 'ilyes', 'Bahlagui', 1, '2', 'ilyesbahlagui@gmail.com', NULL, 'Natsudu30300.', NULL, '2021-09-27'),
(8, NULL, 1, 'Junior', 'SAFA', 10, '1', 'uhoui@gmail.com', NULL, '$2y$10$vhb7cDhFSWcQOJ.Ox.qBtOX2AG3heN/EDzGPwS1b6Hfa2HfPhhJza', NULL, '2021-09-27'),
(20, NULL, 1, 'jijou', 'jikououou', 0, '0', 'jijou@gmail.com', NULL, '$2y$10$i2s8q4Wj3eVmGRl.WQHwqOoHDgHx5vcQLsFqeIghaJ1XjZFgjdDNy', NULL, '2021-10-15'),
(21, NULL, 1, 'ilyes', 'Bahlagui', 0, '2', 'ilyesbahlagui@gmail.com', NULL, '$2y$10$gDiae39AARenNrUTCqN5bedPsvzCtKdCfZDR8kNu.T.UxouzR6E1y', NULL, '2021-10-19'),
(22, NULL, 1, 'ilyes', 'Bahlagui', 0, '0', 'ilyesbahlagui@gmail.com', NULL, '$2y$10$2AJs5VjzsX.ZIDDV03.xauFpLmu/VUy/r9xghb2hvT8sXSVUfsZ.C', NULL, '2021-10-19'),
(23, NULL, 1, 'ilyes', 'Bahlagui', 0, '1', 'ilyesbahlagui@gmail.com', NULL, '$2y$10$fSIAJp4uBHb.8AApIa8queU5CizQPKABFbtd2PEgriiMhknu7VeLa', NULL, '2021-10-19'),
(24, NULL, 1, 'ilyes', 'Bahlagui', 0, '1', 'ilyesbahlagui@gmail.com', NULL, '$2y$10$BLRIT.VGKnhH6isEehXPyu1QhUulrlfTK5h9jkfMaYG..MQX76Cny', NULL, '2021-10-19');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `id_ville` int(11) NOT NULL AUTO_INCREMENT,
  `id_pays` int(11) NOT NULL,
  `nom_ville` varchar(100) NOT NULL,
  `code_postale` int(11) NOT NULL,
  PRIMARY KEY (`id_ville`),
  KEY `VILLE_PAYS_FK` (`id_pays`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id_ville`, `id_pays`, `nom_ville`, `code_postale`) VALUES
(1, 1, 'Paris', 75000),
(2, 1, 'Lyon', 69000),
(3, 1, 'Marseille', 13000),
(4, 1, 'Nimes', 30000),
(5, 1, 'Montpellier', 34000),
(6, 1, 'Avignon', 84000);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `COMMANDE_ETAT_COMMANDE0_FK` FOREIGN KEY (`id_etat_commande`) REFERENCES `etat_commande` (`id_etat_commande`),
  ADD CONSTRAINT `COMMANDE_UTILISATEUR_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `commande__produit`
--
ALTER TABLE `commande__produit`
  ADD CONSTRAINT `COMMANDE__PRODUIT_COMMANDE0_FK` FOREIGN KEY (`id_commande`) REFERENCES `commande` (`id_commande`),
  ADD CONSTRAINT `COMMANDE__PRODUIT_PRODUIT_FK` FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id_produit`);

--
-- Contraintes pour la table `contenu`
--
ALTER TABLE `contenu`
  ADD CONSTRAINT `CONTENU_TYPE_CONTENU_FK` FOREIGN KEY (`id_type_contenu_fk`) REFERENCES `type_contenu` (`id_type_contenu`);

--
-- Contraintes pour la table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `PAGE_UTILISATEUR_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `PLANNING_UTILISATEUR_FK` FOREIGN KEY (`id_utilisateur_fk`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `PRODUIT_FAMILLE_FK` FOREIGN KEY (`id_famille_fk`) REFERENCES `famille` (`id_famille`),
  ADD CONSTRAINT `PRODUIT_TVA0_FK` FOREIGN KEY (`id_tva_fk`) REFERENCES `tva` (`id_tva`);

--
-- Contraintes pour la table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `STAFF_UTILISATEUR_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `type_contenu`
--
ALTER TABLE `type_contenu`
  ADD CONSTRAINT `TYPE_CONTENU_UTILISATEUR_FK` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `UTILISATEUR_ROLE0_FK` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`),
  ADD CONSTRAINT `UTILISATEUR_VILLE_FK` FOREIGN KEY (`id_ville_fk`) REFERENCES `ville` (`id_ville`);

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `VILLE_PAYS_FK` FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id_pays`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
