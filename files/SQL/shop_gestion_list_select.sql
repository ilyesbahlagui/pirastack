select id_produit, id_famille_fk, id_tva_fk, nom_produit, prix_unitaire as prix ,
description_produit as description ,tva.taux_tva as tva, famille.nom_famille as nom_categorie, stock_produit as stock
FROM produit 
inner join famille on famille.id_famille=produit.id_famille_fk
inner join tva on tva.id_tva=produit.id_tva_fk
WHERE 1
order by id_famille_fk
