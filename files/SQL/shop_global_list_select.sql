select id_produit, id_famille_fk, id_tva_fk,nom_produit, prix_unitaire,description_produit as description ,tva.taux_tva, famille.nom_famille , stock_produit
from produit 

inner join tva on tva.id_tva=produit.id_tva_fk
inner join famille on famille.id_famille=produit.id_famille_fk
-- where stock_produit>0 and id_produit in(@id_produit)
where 1
order by id_famille_fk and id_produit
limit  @nb_Art , @nb_Page_Art